////////////////////////////////////////////////////////
/* Button functions, to display instructions on click */
////////////////////////////////////////////////////////
// Get the modal
var modal_instructions = document.getElementById('overlay_instructions');
var modal_warning = document.getElementById('overlay_warning');
// Get the button that opens the modal
var btn = document.getElementById("instructions");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function show_instructions() {
    modal_instructions.style.display = "block";
    //console.log('Button was just pressed');
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    //console.log("CLOSE IT");
    //modal_instructions.style.display = "none";
}

function close_modal(lever) {
    //console.log("CLOSE IT");
    if(lever == 0 ){
        fadeOutEffect(modal_instructions);
    }
    else if( lever == 1 ){
        fadeOutEffect(modal_warning);
    }
   // modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal_instructions) {
        modal_instructions.style.animationDirection="alternate";
       // modal_instructions.style.display = "none";
    }
    else if (event.target == modal_warning){
        modal_warning.style.display = "none";
    }
}

