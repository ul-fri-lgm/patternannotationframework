function nextPage() {
    if(page != vrvToolkit.getPageCount()){
        page = page + 1;
        loadPage();
    }
};

function prevPage() {
    if(page != 1){
        page = page - 1;
	    loadPage();
    }	  	
};

function firstPage() {
	page = 1;
	loadPage();
};

function lastPage() {
	page = vrvToolkit.getPageCount();
	loadPage();
};

function applyZoom() {
	setOptions();
	vrvToolkit.redoLayout();

	page = 1;
	loadPage();
}

function zoomOut() {
	if (zoom < 50) {
		return;
	}

	zoom = zoom / 2;
	applyZoom();
}

function zoomIn() {
	if (zoom > 80) {
		return;
	}

	zoom = zoom * 2;
	applyZoom();
}

// if window is resized, redo svg element
$(window).resize(function(){
	setOptions();
	vrvToolkit.redoLayout();
    loadPage();
});

function processBasicEvents(event) {
    if (event.ctrlKey && (event.keyCode == 37)) {
        firstPage();
    }
    else if (event.keyCode == 37) {
        prevPage();
    }
    else if (event.ctrlKey && (event.keyCode == 39)) {
        lastPage();
    }
    else if (event.keyCode == 39) {
        nextPage();
    }
    // see http://www.javascripter.net/faq/keycodes.htm
    else if ((event.keyCode == 107) || (event.keyCode == 187) || (event.keyCode == 61)) {
        zoomIn();
    }
    else if ((event.keyCode == 109) || (event.keyCode == 189) || (event.keyCode == 173)) {
        zoomOut();
    }
}

// Control loading overlay
$(window).on('load', function () {
    //$('#loading_overlay').fadeOut(1000);
    //document.getElementById("overlay_instructions").style.display = "block";
    var fadeTarget = document.getElementById("loading_overlay");
    fadeOutEffect(fadeTarget);
    
});


function fadeOutEffect(fadeTarget) {
    
    var fadeEffect = setInterval(function () {
        if (!fadeTarget.style.opacity) {
            fadeTarget.style.opacity = 1;
        }
        if (fadeTarget.style.opacity > 0) {
            
            fadeTarget.style.opacity -= 0.05;
            
        } else {
            
            clearInterval(fadeEffect);
            fadeTarget.style.display = "none";
        }
    }, 50);
    //document.getElementById("loading_overlay").style.display = "none";
}

