from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Pattern, Profile, UserBehaviour
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import NewUserForm, ProfileForm
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse
from django.contrib.staticfiles.templatetags.staticfiles import static
import os 
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView, PasswordContextMixin

from django.contrib.auth.forms import PasswordResetForm
from django.urls import reverse_lazy
from django.contrib.auth.tokens import default_token_generator
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect

import random
import json

def is_registered(user):
    return user.is_authenticated

def is_not_logged_in(user):
    return user.is_anonymous

# Create your views here.
@login_required(login_url='/login')
#@user_passes_test(is_registered)
def index(request):
    context = {}
    list_of_songs = get_song_names(request)
    song_names = []

    for song in list_of_songs:
        tmp = song.split('.')[0]
        tmp = tmp.split('_')[1:]
        tmp = " ".join(tmp)
        song_names.append(tmp)

    
    context['list_of_songs'] = list_of_songs
    context['song_names'] = song_names
    return render(request=request, 
                  template_name="index.html", context=context)

@login_required(login_url='/login')
def update_user_song_list(request):
    current_user = request.user
    
    song_list = current_user.profile.song_list
    #song_names = request.session.get('song_names')
    #print(song_names)
    #print(current_user)
    #print(song_list)
    #print("updating yo")
    data = {
        'code': "200",
        'user': current_user.id,
        'song_list': song_list,
        'song_names': None,
    }
    return JsonResponse(data)

# REGISTER
@user_passes_test(is_not_logged_in, login_url='main:index')
def register(request):
    if (request.method == "POST"):
        form = NewUserForm(request.POST)
        profile_form = ProfileForm(request.POST)

        if ( form.is_valid() and profile_form.is_valid() ):
            #print("we are here")
            # save user from form

            user = form.save(commit=False)
            user.is_active = False
            user.save()
            #username = form.cleaned_data.get('username')
            # save profile from form
            for field in profile_form.changed_data:
                setattr(user.profile, field, profile_form.cleaned_data.get(field))
            #user.profile.instrument = profile_form.cleaned_data.get('instrument')
            #user.profile.age = profile_form.cleaned_data.get('age')
            user.profile.save()  

            # SEND EMAIL
            current_site = get_current_site(request)
            #temporary_domain = "mydomain.com"
            mail_subject = 'Activate your Pattern Annotator account.'
            message = render_to_string('acc_active_email.html', {
                                    'user': user,
                                    'domain': current_site,#current_site.domain,
                                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                                    'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.info(request, "A confirmation link was sent to the provided email adress.")
            messages.info(request, "Please check your inbox to complete the registration process.")

            #messages.info(request, "Please confirm your email address to complete the registration")
            # message user and login
            #messages.success(request, f"New Account Created: {username}")
            #login(request, user)
            #messages.info(request, f"You are now logged in as {username}")
            #return redirect("main:index")
            
        else:
            for field, errors in sorted(form.errors.items(), reverse=True):
                #print("field")
                for error in errors:
                    #print("error:")
                    #print(error)
                    #messages.error(request, form.fields[field].label + ": " + error )
                    messages.error(request, error)
    else:
        form = NewUserForm()
        profile_form = ProfileForm()
    return render(request,
                  "register.html",
                  context={"form":form, 'profile_form':profile_form})


@user_passes_test(is_not_logged_in, login_url='main:index')
def password_reset(request):
    if (request.method == "POST"):
        form = PasswordResetForm(request.POST)
        profile_form = ProfileForm(request.POST)

        if ( form.is_valid() and profile_form.is_valid() ):
            #print("we are here")
            # save user from form

            user = form.save(commit=False)
            user.is_active = False
            user.save()
            #username = form.cleaned_data.get('username')
            # save profile from form
            for field in profile_form.changed_data:
                setattr(user.profile, field, profile_form.cleaned_data.get(field))
            #user.profile.instrument = profile_form.cleaned_data.get('instrument')
            #user.profile.age = profile_form.cleaned_data.get('age')
            user.profile.save()  

            # SEND EMAIL
            current_site = get_current_site(request)
            #temporary_domain = "mydomain.com"
            mail_subject = 'Activate your Pattern Annotator account.'
            message = render_to_string('acc_active_email.html', {
                                    'user': user,
                                    'domain': current_site,#current_site.domain,
                                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                                    'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.info(request, "A confirmation link was sent to the provided email adress.")
            messages.info(request, "Please check your inbox to complete the registration process.")

            #messages.info(request, "Please confirm your email address to complete the registration")
            # message user and login
            #messages.success(request, f"New Account Created: {username}")
            #login(request, user)
            #messages.info(request, f"You are now logged in as {username}")
            #return redirect("main:index")
            
        else:
            for field, errors in sorted(form.errors.items(), reverse=True):
                #print("field")
                for error in errors:
                    #print("error:")
                    #print(error)
                    #messages.error(request, form.fields[field].label + ": " + error )
                    messages.error(request, error)
    else:
        form = NewUserForm()
        profile_form = ProfileForm()
    return render(request,
                  "register.html",
                  context={"form":form, 'profile_form':profile_form})

# LOGOUT
def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("main:login")




# LOGIN
@user_passes_test(is_not_logged_in, login_url='main:index')
def login_request(request):
    if (request.method == "POST"):
        form = AuthenticationForm(request, data=request.POST)
        if (form.is_valid()):

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            if (user is not None):
                login(request, user)
                messages.info(request, "You are now logged in as {}.".format(username)) #f"You are now logged in as {username}")
                return redirect("main:index")
        else:
            messages.error(request, "Invalid username or password.")


    form = AuthenticationForm()
    return render(request, 
                  "login.html",
                  {"form": form})

# GET MEI FILENAMES
@login_required(login_url='/login')
def get_filenames(request):
    song_names = get_song_names(request)
    data = {
        'file_list': song_names
    }

    return JsonResponse(data)

@login_required(login_url='/login')
def get_song_names(request):
    app_path = os.path.dirname(os.path.abspath(__file__))
    static_path = static('mei/')
    path = app_path + static_path
    song_names = os.listdir(path)
    song_names.sort()
    random.seed(request.user.id)
    random.shuffle(song_names)
    return song_names


# GET RESULT FILENAMES
@login_required(login_url='/login')
def get_result_filenames(request):
    app_path = os.path.dirname(os.path.abspath(__file__))
    static_path = static('result/')
    path = app_path + static_path
    song_names = os.listdir(path)
    data = {
        'file_list': song_names
    }
    return JsonResponse(data)

# POST XML
@login_required(login_url='/login')
def post_xml_patterns(request):
    
    current_user = request.user
    user_id = current_user.id
    which_song = request.POST.get('which_song', None)
    xml_string_pattern = request.POST.get('xml_string_pattern', None)
    pattern_rank = request.POST.get('pattern_rank', None)
    pattern_timestamp = request.POST.get('pattern_timestamp', None)
    pattern_tag = request.POST.get('pattern_tag', None)
    is_automatic = json.loads(request.POST.get('is_automatic', None))

    print()
    print("user id is: ", user_id)
    print("song id is: ", which_song)

    pat = Pattern(user_id=user_id, song_id=which_song, xml_file=xml_string_pattern, pattern_rank=pattern_rank, pattern_timestamp=pattern_timestamp, pattern_tag=pattern_tag, is_automatic=is_automatic)
    pat.save()
    # also update the profile song_list
    prof = Profile.objects.get(id=current_user.id)
    updated_song_list = prof.song_list
    updated_song_list.append(int(which_song)+1)
    prof.song_list = updated_song_list # change field
    prof.save() # this will update only


    data = {
        'code': "200",
        'user': user_id,
        'which_song': which_song
    }
    return JsonResponse(data)

# POST User Behaviour
@login_required(login_url='/login')
def post_user_behaviour(request):
    print("Save user behaviour")
    current_user = request.user
    user_id = current_user.id
    which_song = request.POST.get('which_song', None)
    midi_start_times = request.POST.getlist('midi_start_times[]', None)
    midi_start_times = list(map(int, midi_start_times))
    midi_stop_times = request.POST.getlist('midi_stop_times[]', None)
    midi_stop_times = list(map(int, midi_stop_times))
    
    number_of_annotations = request.POST.get('number_of_annotations', None)
    number_of_deletions = request.POST.get('number_of_deletions', None)
    
    print(midi_start_times)
    beh = UserBehaviour(user_id=user_id, song_id=which_song, midi_start_times=midi_start_times, midi_stop_times=midi_stop_times, number_of_annotations=number_of_annotations, number_of_deletions=number_of_deletions)
    beh.save()
    
    data = {
        'code': "200",
        'user': user_id,
        'which_song': which_song
    }
    return JsonResponse(data)

# email verification
@user_passes_test(is_not_logged_in, login_url='main:index')
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.info(request, "Thank you for your email confirmation.")#f"Thank you for your email confirmation")
        messages.info(request, "You are now logged in as {}.".format(user.username))
        return redirect('main:index')
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')





class MyPasswordResetView(PasswordContextMixin, FormView):
   email_template_name = 'registration/password_reset_email.html'
   extra_email_context = None
   form_class = PasswordResetForm
   from_email = None
   html_email_template_name = None
   subject_template_name = 'registration/password_reset_subject.txt'
   success_url = 'password_reset_done'
   template_name = 'registration/password_reset_form.html'
   title = 'Password reset'
   token_generator = default_token_generator

   @method_decorator(csrf_protect)
   def dispatch(self, *args, **kwargs):
       return super().dispatch(*args, **kwargs)

   def form_valid(self, form):
       opts = {
           'use_https': self.request.is_secure(),
           'token_generator': self.token_generator,
           'from_email': self.from_email,
           'email_template_name': self.email_template_name,
           'subject_template_name': self.subject_template_name,
           'request': self.request,
           'html_email_template_name': self.html_email_template_name,
           'extra_email_context': self.extra_email_context,
       }
       #We've emailed you instructions for setting your password. You should receive the email shortly!
       # add messages
       messages.info(self.request, "Instructions for resetting the password have been emailed to you.")
       form.save(**opts)
       return super().form_valid(form)




def mypassword_reset_view(request):
    view = MyPasswordResetView.as_view(template_name='custom_menus/password_reset_form.html',email_template_name='custom_menus/password_reset_email.html',subject_template_name='custom_menus/password_reset_subject.txt',success_url=request.path)(request)
    return view