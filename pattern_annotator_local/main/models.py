from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from jsonfield import JSONField

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    age = models.PositiveIntegerField(null=True)
    instrument = models.CharField(max_length=200, null=True)
    instrument_years = models.PositiveIntegerField(null=True)
    theory_years = models.PositiveIntegerField(null=True)
    country = models.CharField(max_length=200,null=True)
    university = models.CharField(max_length=200, null=True)
    programme = models.CharField(max_length=200,null=True)
    year_of_study = models.CharField(max_length=200, null=True, default=None)
    song_list = JSONField(default=list)


    def __str__(self):
        return "{}: {}, {}".format(self.user.username, self.instrument, self.age)
        
class Pattern(models.Model):
    #pattern_id = models.AutoField(primary_key=True, default=0)
    user_id = models.IntegerField(null=True)
    song_id = models.IntegerField(null=True)
    xml_file = models.TextField(null=True) # blob of text
    pattern_rank = models.IntegerField(null=True)
    pattern_timestamp = models.IntegerField(null=True)
    pattern_tag = models.CharField(max_length=150, null=True)
    is_automatic = models.BooleanField(null=True)
    # override string method, when printed with string
    def __str__(self):
        return "Pattern({}): user({}) on song({}), with rank({}) and tag[{}]".format(self.id, self.user_id, self.song_id, self.pattern_rank, self.pattern_tag)

class UserBehaviour(models.Model):
    user_id = models.IntegerField(null=True)
    song_id = models.IntegerField(null=True)
    midi_start_times = JSONField(default=list)
    midi_stop_times = JSONField(default=list)
    number_of_annotations = models.IntegerField(null=True)
    number_of_deletions = models.IntegerField(null=True)
    
    # override string method, when printed with string
    def __str__(self):
        return "Behaviour of User({}) on Song({})".format(self.user_id, self.song_id)
        