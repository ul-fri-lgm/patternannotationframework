"""pattern_annotator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

app_name = 'main'
urlpatterns = [
    path('', views.index, name="index"),
    path('register', views.register, name="register"),
    path('logout', views.logout_request, name="logout"),
    path('login', views.login_request, name="login"),
    url(r'^ajax/get_filenames/$', views.get_filenames, name='get_filenames'),
    url(r'^ajax/get_result_filenames/$', views.get_result_filenames, name='get_result_filenames'),
    url(r'^ajax/post_xml_patterns/$', views.post_xml_patterns, name='post_xml_patterns'),
    url(r'^ajax/post_user_behaviour/$', views.post_user_behaviour, name='post_user_behaviour'),
    url(r'^ajax/update_user_song_list/$', views.update_user_song_list, name='update_user_song_list'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    url(r'password_reset/$', views.mypassword_reset_view, name='password_reset'),
    #url(r'password_reset/$',auth_views.PasswordResetView.as_view(template_name='custom_menus/password_reset_form.html',email_template_name='custom_menus/password_reset_email.html',subject_template_name='custom_menus/password_reset_subject.txt',success_url='/password_reset/done/'), name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(template_name="custom_menus/password_reset_done.html"), name='password_reset_done'),
    #url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.PasswordResetConfirmView.as_view(template_name="custom_menus/password_reset_confirm.html", success_url='/reset/done/'), name='password_reset_confirm'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.PasswordResetConfirmView.as_view(template_name="custom_menus/password_reset_confirm.html", success_url='/login'), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(template_name="custom_menus/password_reset_complete.html"), name='password_reset_complete'),
]
