from django.contrib import admin
from .models import Profile, Pattern, UserBehaviour
from django.contrib.auth.models import User
from import_export.admin import ImportExportModelAdmin

# Register your models here.
#admin.site.register(Profile)
#admin.site.register(Pattern)

class UserAdmin(ImportExportModelAdmin):
    pass

class ProfileAdmin(ImportExportModelAdmin):
    pass

class PatternAdmin(ImportExportModelAdmin):
    pass

class BehaviourAdmin(ImportExportModelAdmin):
    pass

# USER
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
# PROFILE
admin.site.register(Profile, ProfileAdmin)
# PATTERN
admin.site.register(Pattern, PatternAdmin)
# BEHAVIOUR
admin.site.register(UserBehaviour, BehaviourAdmin)