# Generated by Django 2.2.1 on 2020-02-16 19:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20200216_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pattern',
            name='pattern_tag',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
