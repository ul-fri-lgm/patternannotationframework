# Generated by Django 2.2.1 on 2019-12-06 15:10

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='song_list',
            field=jsonfield.fields.JSONField(default=list),
        ),
    ]
