# Generated by Django 2.2.1 on 2020-02-27 00:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20200227_0145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='year_of_study',
        ),
    ]
