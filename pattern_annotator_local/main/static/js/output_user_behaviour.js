// script to output pattern in an xml format

function output_user_behaviour_to_DB(){

    if (patterns_start.length == 0){
        return;
    }
    //console.log("POST USER BEHAVIOUR FOR ID:")
    //console.log(Number(filenames[songID].split("_")[0]));
    let song_global_id = Number(filenames[songID].split("_")[0]) -1;//songID;
    let song_local_id = songID;
    
    // get the id for the note at which the pattern starts
    let behaviour_midi_start = midi_start_times;
    let behaviour_midi_stop = midi_stop_times;
    //console.log("BEHAVIOUR:");
    //console.log(behaviour_midi_start);
    //console.log(behaviour_midi_stop);
    let behaviour_number_annotations = number_of_annotations;
    let behaviour_number_deletions = number_of_deletions;

    $.ajax({
        url: '/ajax/post_user_behaviour/',
        datatype: 'json',
        type: "POST",
        data: {csrfmiddlewaretoken: window.CSRF_TOKEN, which_song: song_global_id, midi_start_times: behaviour_midi_start, midi_stop_times: behaviour_midi_stop, number_of_annotations: behaviour_number_annotations, number_of_deletions: behaviour_number_deletions},
        success: function(result) {
            console.log("Call to post_user_behaviour succeded: ");
            console.log(result)
        },
        error: function(req, err){ 
            console.log("Call to post_user_behaviour failed; " + err);
        },
    });
    
}