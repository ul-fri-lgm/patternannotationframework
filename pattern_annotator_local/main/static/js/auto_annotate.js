// Idea: 
// have a rectangle, which on click automatically annotates patterns that are the same
// on click get the pattern ID
// -> get pattern start and end time
// -> find which patterns are on that interval start - end, these are candidates
// -> then check through the candidates and check if all notes correspond
// -> if a note does not -> break


function check_if_element_at_time_and_index_in_pat(atm_time, note_index, all_pattern_notes){
    
    sub_element = vrvToolkit.getElementsAtTime(atm_time).notes;
    
    sub_element_id = "";
    if(sub_element == undefined){
        return false;
    }
    else if (sub_element.length == 1){
        sub_element_id = sub_element[0];
    }
    else{
        sub_element_id = sub_element[1];
    }
    sub_element = vrvToolkit.getElementAttr(sub_element_id);
    sub_element_dur = vrvToolkit.getElementAttr(sub_element_id).dur;
    
    if(sub_element.pname != all_pattern_notes[note_index].pname ||  sub_element.dur != all_pattern_notes[note_index].dur || sub_element.oct != all_pattern_notes[note_index].oct){
        //console.log("here");
        return false;
        
    }
    else{
        return true;
    }
}
function auto_annotate(start_time, end_time, note_start_ids, note_end_ids, a_staff_num, pattern_id){
    // if a pattern is not done yet
    if(clicked == true){
        show_warning(0);
        return
    }
    //console.log(pattern_id);
    // QUICK FIX
    // delete current pattern to then redraw it
    for( i = 0; i < patterns_id.length; i++){
        //console.log("REDO:, ", i, " ", patterns_id[i]);
        //delete_svg_rect(patterns_id[i]);
        delete_all_svg_rects();
        //delete_svg_rect(patterns_id.indexOf(patterns_id[i]));
    }
    

    if (start_time == undefined || end_time == undefined){
        return;
    }
    
    // subfunction that adds all occurances of the pattern to the global arrays
    add_all_same_patterns(note_start_ids[patterns_id.indexOf(pattern_id)], note_end_ids[patterns_id.indexOf(pattern_id)], a_staff_num, pattern_id);

    reload_patterns(0);
    reload_svg_rect(note_start_ids, note_end_ids);
    
    
    // REDONE
    /*
    // note for each, find two consecutive notes -> get the time
    // or why not go across all like this and just find them this way
    // CANNOT USE JUST THIS, IT ONLY SHOWS THE CURRENT PAGE,
    // BUT WE CAN USE IT TO GET THE TIME RATIO
    
    console.log("START: ", start_time, "  END:", end_time);
    tmp_time_0 = 0;
    tmp_time_1 = 0;
    tmp_first_dur = 0;
    tmp_index = 0;
    pattern_length = end_time - start_time;

    previous_time = 0;
    $(".note").each(function(i) {
        //console.log($(this));
        if(tmp_index == 0){
            tmp_id = ($(this).attr("id"));
            //console.log(tmp_id);
            tmp_time_0 = vrvToolkit.getTimeForElement(tmp_id);
            //console.log("A", tmp_time_0);
            tmp_first_dur = vrvToolkit.getElementAttr(tmp_id).dur;
            //console.log("B", tmp_first_dur);
            if(tmp_first_dur != undefined){
                tmp_index += 1;
            }
            
        }
        else if(tmp_index == 1){
            //console.log($(this).attr("id"));
            tmp_id = ($(this).attr("id"));
            //console.log(tmp_id);
            //console.log(vrvToolkit.getElementAttr(tmp_id));
            //console.log("S time: ",vrvToolkit.getTimeForElement(tmp_id));
            
            tmp_time_1 = vrvToolkit.getTimeForElement(tmp_id);    
            
            
            if(tmp_time_1 != tmp_time_0){
                tmp_index += 1;
            }
            
        }
        else{
            return;
        }

        
    });

    min_delta_time = tmp_time_1 - tmp_time_0;
    min_delta_dur = 16;

    //console.log("HERE",min_delta_time, "  ", tmp_first_dur);
    
    while(tmp_first_dur < min_delta_dur){
        min_delta_time = min_delta_time / 2;
        tmp_first_dur = tmp_first_dur * 2;
    }
    if(tmp_first_dur == undefined){
        return;
    }
    //console.log(min_delta_time, "  ", tmp_first_dur);

    // first save the pattern that was selected as an array
    // then go through time and mark it

    var all_pattern_notes = [];
    tmp_time = start_time +1;
    while(tmp_time <= end_time+1){
        
        //console.log("AT TIME: ", tmp_time);
        tmp_element = vrvToolkit.getElementsAtTime(tmp_time).notes;
        //console.log("HERE:", tmp_element);
        tmp_element_id = "";
        if (tmp_element.length == 1){
            tmp_element_id = tmp_element[0];
        }
        else{
            tmp_element_id = tmp_element[1];
        }
        tmp_element = vrvToolkit.getElementAttr(tmp_element_id);
        all_pattern_notes.push(tmp_element);

        tmp_element_dur = vrvToolkit.getElementAttr(tmp_element_id).dur;
        //if(tmp_element_dur == undefined){
        //    tmp_time = tmp_time + min_delta_time;
        //}
        tmp_time = tmp_time + min_delta_time * min_delta_dur/tmp_element_dur;
        //console.log("GO TO: ", tmp_time , "   ", min_delta_time, "    ", min_delta_dur/tmp_element_dur) ;
    }
    //console.log("Annotated pattern:");
    //console.log(all_pattern_notes);


    // elements in time go 500 for quarter notes, 250 for eigth notes
    // find minimal length note on page and go through I suppose?
    
    checking_first_element = true;
    tmp_time = 1;
    //console.log("SEARCH FOR: ", all_pattern_notes[0].pname );
    //console.log("END TIME:", player_midi.endTime);
    while(tmp_time <= player_midi.endTime){
        //console.log("AT TIME: ", tmp_time);
        
        tmp_element = vrvToolkit.getElementsAtTime(tmp_time).notes;
        //console.log(tmp_element);
        tmp_element_id = "";

        if (tmp_element.length == 1){
            tmp_element_id = tmp_element[0];
        }
        else{
            tmp_element_id = tmp_element[1];
        }
        //console.log(tmp_element_id);
        if(tmp_element_id != undefined){
            //console.log("here?");
            tmp_element = vrvToolkit.getElementAttr(tmp_element_id);
            tmp_element_dur = vrvToolkit.getElementAttr(tmp_element_id).dur;

            if(tmp_element_dur == undefined){
                //console.log("aa?");
                tmp_time = tmp_time + min_delta_time;
                continue;
            }
            // if first note matches call go into while and try if it fills the whole pattern, otherwise return
            //console.log("note:", tmp_element.pname);
            last_pat_index = all_pattern_notes.length - 1
            // if not the same position as annotated
            // if same note name and duration
            //if(tmp_time == start_time +1){
            //    console.log("STARTING POSITION YO")
            //}
            if(tmp_time != start_time+1 && tmp_element.pname == all_pattern_notes[0].pname && tmp_element.dur == all_pattern_notes[0].dur && tmp_element.oct == all_pattern_notes[0].oct){
                //console.log("FOUND START MATCH");
                sub_time = tmp_time;
                pat_note_index = 0;
                //// check if tmp_time + pat_len == last note from pattern
                tmp_end_time = tmp_time + pattern_length;
                
                

                //sub_time = tmp_time  + min_delta_time * min_delta_dur/sub_element_dur;
                previous_add = 0;
                //console.log("Check", tmp_end_time)
                condition_a = check_if_element_at_time_and_index_in_pat(tmp_end_time, last_pat_index, all_pattern_notes)
                
                //console.log("Log it");
                while(true && condition_a){
                    //console.log("__");
                    //console.log("a")
                    if( pat_note_index >= all_pattern_notes.length){
                        end_time = sub_time - previous_add;
                        
                        
                        console.log("Add automatic pattern from: ", tmp_time-1, "  to:", end_time-1);
                        
                        // if start and end id is already a combination in patterns
                        if (note_start_ids.includes(tmp_element_id) && note_end_ids.includes(sub_element_id) ){
                            //console.log("HERE I AM");
                            tmp_check_end = note_end_ids.indexOf(sub_element_id);
                            tmp_check_start = note_start_ids.indexOf(tmp_element_id);
                            
                            //console.log(tmp_check_end , "   ", tmp_check_start  );
                            if(Number(tmp_check_end) == Number(tmp_check_start)){
                                //console.log("BREAK IT YO");
                                break;
                            }
                        }
                        
                        
                        // add pattern
                        // from time = tmp_time - 1
                        // to time = tmp_time + sub_time - 1
                        patterns_start.push(tmp_time-1);
                        patterns_end.push(end_time-1);
                        patterns_staff.push(a_staff_num);
                        patterns_id.push(next_pattern_id);
                        //console.log(note_start_ids);
                        //console.log(tmp_element_id);
                        note_start_ids.push(tmp_element_id);
                        note_end_ids.push(sub_element_id);
                        //patterns_rank.push(initial_rank_value);
                        patterns_rank.push(patterns_rank[patterns_id.indexOf(pattern_id)]);
                        
                        patterns_colours.push(pattern_id);
                        //console.log(pattern_id);
                        //console.log(patterns_id);
                        patterns_tags.push(patterns_tags[patterns_id.indexOf(pattern_id)]);
                        next_pattern_id = next_pattern_id + 1;
                        break;
                    }
                    sub_element = vrvToolkit.getElementsAtTime(sub_time).notes;
                    sub_element_id = "";
                    if (sub_element.length == 1){
                        sub_element_id = sub_element[0];
                    }
                    else{
                        sub_element_id = sub_element[1];
                    }
                    console.log("ID: ",sub_element_id);
                    sub_element = vrvToolkit.getElementAttr(sub_element_id);
                    sub_element_dur = vrvToolkit.getElementAttr(sub_element_id).dur;
                    
                    if(sub_element.pname != all_pattern_notes[pat_note_index].pname ||  sub_element.dur != all_pattern_notes[pat_note_index].dur || sub_element.oct != all_pattern_notes[pat_note_index].oct ){
                        console.log("Not the pattern we are looking for.");
                        //console.log(sub_element.pname, "   ",all_pattern_notes[pat_note_index].pname );
                        //console.log(sub_element.dur, "    " ,all_pattern_notes[pat_note_index].dur );
                        break;
                    }
                    previous_add = min_delta_time * min_delta_dur/sub_element_dur;
                    sub_time = sub_time + min_delta_time * min_delta_dur/sub_element_dur;
                    pat_note_index = pat_note_index + 1;
                }
            }
        
            tmp_time = tmp_time + min_delta_time * min_delta_dur/tmp_element_dur;       
        }
        else{
            tmp_time = tmp_time + min_delta_time;
        }
        
        
        
    }
    
    reload_patterns(0);
    reload_svg_rect(note_start_ids, note_end_ids);
    tmp_time = tmp_time + min_delta_time * min_delta_dur/tmp_element_dur;
    */
    //console.log("GO TO: ", tmp_time , "   ", min_delta_time, "    ", min_delta_dur/tmp_element_dur) ;
}


function add_a_pattern(note_start_id, note_end_id, start_time, end_time, a_staff_num, pattern_id){
    // add pattern
    // from time = tmp_time - 1
    // to time = tmp_time + sub_time - 1
    patterns_id.push(next_pattern_id);
    note_start_ids.push(note_start_id);
    patterns_start.push(start_time);
    patterns_end.push(end_time);
    patterns_staff.push(a_staff_num);
    note_end_ids.push(note_end_id);
    patterns_rank.push(patterns_rank[patterns_id.indexOf(pattern_id)]);
    patterns_colours.push(pattern_id);
    patterns_tags.push(patterns_tags[patterns_id.indexOf(pattern_id)]);
    patterns_is_automatic.push(true);
    patterns_timestamps.push( Math.floor(Date.now() / 1000) );
    
    next_pattern_id = next_pattern_id + 1;
}

function add_all_same_patterns(start_id, end_id, a_staff_num, pattern_id){

    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(xml_file,"text/xml");

    var root = xmlDoc.getElementsByTagName("section")[0];   //find section, use it as the root
    var all_notes = xmlDoc.getElementsByTagName("note");
    
    // get start and end note:
    var start_node_i = -1;
    var end_node_i = -1;
    var length_apart = -1;
    
    for (var i = 0; i < all_notes.length; i++){
        var tmp_node = all_notes[i];
        if(tmp_node.getAttribute("xml:id") == start_id){
            //console.log("I FOUND STARTING:");
            //console.log(tmp_node);
            start_node_i = i;
            length_apart = 0;
        }
        if(tmp_node.getAttribute("xml:id") == end_id){
            //console.log("I FOUND END:");
            //console.log(tmp_node);
            end_node_i = i;
            break;
        }
        length_apart += 1;
    }
    
    // get attributes we want from start and end
    var start_node_pname = all_notes[start_node_i].getAttribute("pname");
    var start_node_dur = all_notes[start_node_i].getAttribute("dur");
    var start_node_accid = get_node_accidental(all_notes[start_node_i]);

    //console.log("Accid:", start_node_accid);
    var end_node_pname = all_notes[end_node_i].getAttribute("pname");
    var end_node_dur = all_notes[end_node_i].getAttribute("dur");
    var end_node_accid = get_node_accidental(all_notes[end_node_i]);
    

    var possible_pattern_starts = [];
    // go through all notes, find all same starting notes, that are same, and same length apart
    for (var i = 0; i < all_notes.length; i++){
        var tmp_node = all_notes[i];
        //get_node_accidental(tmp_node);
        
        // if same as start node, but not literally the starting node!
        if( tmp_node.getAttribute("pname") == start_node_pname 
           && tmp_node.getAttribute("dur") == start_node_dur 
           && get_node_accidental(tmp_node) == start_node_accid
           && tmp_node.getAttribute("xml:id") != start_id 
           ){

            // if length_apart away -> is the end node:
            if(i+length_apart < all_notes.length){
                var possible_end = all_notes[i+length_apart];
                // if end node matches
                if(possible_end.getAttribute("pname") == end_node_pname && possible_end.getAttribute("dur") == end_node_dur && get_node_accidental(possible_end) == end_node_accid ){
                    possible_pattern_starts.push(i);
                }
            }
        

        }

        
    }
    //console.log(possible_pattern_starts);
    var automatic_patterns = []
    //console.log("len_apart:", length_apart);
    // go across all possible pattern starts (numbers inside) 
    for (var i = 0; i < possible_pattern_starts.length; i++){
        var possible_start_i = possible_pattern_starts[i];    
        var end_node = all_notes[start_node_i + length_apart];
    
        // skip first one, we already know it's the start
        for (var j = 1; j <= length_apart; j++){
            //console.log(start_node_i + j);
            //console.log(possible_start_i + j);
            var orig_node = all_notes[start_node_i + j];
            var tmp_node = all_notes[possible_start_i + j];
            
            if(tmp_node.getAttribute("dur") == end_node.getAttribute("dur") && tmp_node.getAttribute("pname") == end_node.getAttribute("pname") && get_node_accidental(tmp_node) == get_node_accidental(end_node)){
                console.log("Automatically add pattern")
                automatic_patterns.push([possible_pattern_starts[i], possible_pattern_starts[i] + length_apart]);
            }

            // if a node does not match, break, we did not find the same pattern 
            else if(tmp_node.getAttribute("dur") != orig_node.getAttribute("dur") || tmp_node.getAttribute("pname") != orig_node.getAttribute("pname") || get_node_accidental(tmp_node) != get_node_accidental(orig_node)){
                //console.log("Not good pattern");
                break;
            }
            

        }
    }
  
    for (var i = 0; i < automatic_patterns.length; i++){
        // get ids
        var tmp_start_id = all_notes[automatic_patterns[i][0]].getAttribute("xml:id");
        var tmp_end_id = all_notes[automatic_patterns[i][1]].getAttribute("xml:id");
        // get times
        var tmp_start_time = vrvToolkit.getTimeForElement(tmp_start_id);
        var tmp_end_time = vrvToolkit.getTimeForElement(tmp_end_id);
        //console.log("From:",  tmp_start_time, " to ", tmp_end_time);
        
        if( check_if_this_pattern_exists(tmp_start_time, tmp_end_time) == false){
            //console.log("heree");
            add_a_pattern(tmp_start_id, tmp_end_id, tmp_start_time, tmp_end_time, a_staff_num, pattern_id);
        }

    }
    
} 

// based on the start and end time, check if this pattern is already annotated
function check_if_this_pattern_exists(start_time, end_time){
    for (var i = 0; i < patterns_start.length; i++){
        if(patterns_start[i] == start_time && patterns_end[i] == end_time){
            return true;
        }
    }
    return false;
}

// get node accidental, null or f/s
function get_node_accidental(node){
    var note_accid = null;
    //console.log("Note:");
    //console.log(node);
    // check if has accid    
    note_accid = node.getAttribute('accid.gis');
    //console.log(note_accid);
    // if not, check if has child
    var children = node.childNodes;
    if(note_accid == null && children.length != 0 ){
        for (var j = 0;  j < children.length; j++){
            if(children[j].tagName == "accid"){
                //console.log(children[j]);
                note_accid = children[j].getAttribute("accid");
                break;
            }
        }
    }

    return note_accid;
}