////////////////////////////////////////////////////////
/* Button functions, to display instructions on click */
////////////////////////////////////////////////////////
// Get the modal
var modal_song_menu = document.getElementById('overlay_song_menu');

// Get the button that opens the modal
var btn = document.getElementById("song_menu");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];


// When the user clicks the button, open the modal 
function show_song_menu() {
    $('#overlay_song_menu').modal('open');
    colour_song_squares();
}

function colour_song_squares(){
    var not_done = document.getElementsByClassName("btn_pick_song_not_done");

    for (var i=0; i < not_done.length; i++){
        // if a song has already been completed -> colour faded
        var django_song_incorrect_id = not_done[i].getAttribute("song_id") -1;
        //console.log(django_song_incorrect_id);
        var correct_song_index = parseInt( filenames[ django_song_incorrect_id ].split("_")[0]); // +-1 ?


        //for (var j=0; i < filenames.length; j++){
        //    if(django_song_incorrect_id == parseInt( filenames[j].split("_")[0] ){
        //         
        //    }
        //}
        
        //if( user_song_list.includes(parseInt(not_done[i].getAttribute("song_id")))){
        if( user_song_list.includes(correct_song_index)){
            not_done[i].onclick = function() {return false;}
            not_done[i].id = "btn_pick_song_done";

            if (! not_done[i].innerHTML.includes(" ✔")){
                not_done[i].innerHTML += " &#10004;";
            }
            //not_done[i].innerHTML += " &#10004;";
            //not_done[i].style.backgroundColor = "var(--color_four)";
            
            //not_done[i].style.cursor = "default";
        }
        
        // if this is the current song we are on -> colour blue
        else if(not_done[i].getAttribute("song_id") == (songID+1)){
            not_done[i].id ="btn_current_song";
            //not_done[i].style.backgroundColor = "var(--color_two)";
        }
        // otherwise this song is yet to be completed and
        // is not the current song, colour darker
        else{
            not_done[i].id = "btn_pick_song_not_done"
            //not_done[i].style.backgroundColor = "var(--color_one)";
        }
        
    }
    
}


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    //console.log("CLOSE IT");
    //modal_instructions.style.display = "none";
}

function close_song_menu() {
    //console.log("CLOSE IT");
    $(modal_song_menu).modal("close");

}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    
    //if (event.target == modal_song_menu) {
    //    modal_song_menu.style.animationDirection="alternate";
    //   // modal_instructions.style.display = "none";
    //}
    //else if (event.target == modal_warning){
    //    modal_warning.style.display = "none";
    //}
}

