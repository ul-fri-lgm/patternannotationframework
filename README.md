**Pattern Annotation Framework (PAF)**

This is the bitbucket repository for the Pattern Annotation Framework, developed in LGM (Faculty of Computer Science - University of Ljubljana). It allows users to submit patterns found in music to the lab for further analysis.

An instance is currently running at http://framework.musiclab.si, using the HEMAN dataset. Feel free to submit your annotations!
--- 

To make new tasks:
- find or write sheet music in the .musicxml format
- use the verovio site to convert the file to .mei
- rename .mei to .xml so that editing before submitting works
---

