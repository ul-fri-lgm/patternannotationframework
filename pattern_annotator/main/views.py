from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Pattern
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import NewUserForm, ProfileForm
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse
from django.contrib.staticfiles.templatetags.staticfiles import static
import os 
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.contrib.auth.models import User

def is_registered(user):
    return user.is_authenticated

def is_not_logged_in(user):
    return user.is_anonymous
    
# Create your views here.
@login_required(login_url='/login')
#@user_passes_test(is_registered)
def index(request):
    return render(request=request, 
                  template_name="index.html")

# REGISTER
@user_passes_test(is_not_logged_in, login_url='main:index')
def register(request):
    if (request.method == "POST"):
        form = NewUserForm(request.POST)
        profile_form = ProfileForm(request.POST)

        if ( form.is_valid() and profile_form.is_valid() ):
            #print("we are here")
            # save user from form

            user = form.save(commit=False)
            user.is_active = False
            user.save()
            #username = form.cleaned_data.get('username')
            # save profile from form
            for field in profile_form.changed_data:
                setattr(user.profile, field, profile_form.cleaned_data.get(field))
            #user.profile.instrument = profile_form.cleaned_data.get('instrument')
            #user.profile.age = profile_form.cleaned_data.get('age')
            user.profile.save()  

            # SEND EMAIL
            current_site = get_current_site(request)
            #temporary_domain = "mydomain.com"
            mail_subject = 'Activate your Pattern Annotator account.'
            message = render_to_string('acc_active_email.html', {
                                    'user': user,
                                    'domain': current_site,#current_site.domain,
                                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                                    'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.info(request, "A confirmation link was sent to the provided email adress.")
            messages.info(request, "Please check your inbox to complete the registration process.")

            #messages.info(request, "Please confirm your email address to complete the registration")
            # message user and login
            #messages.success(request, f"New Account Created: {username}")
            #login(request, user)
            #messages.info(request, f"You are now logged in as {username}")
            #return redirect("main:index")
            
        else:
            for field, errors in sorted(form.errors.items(), reverse=True):
                #print("field")
                for error in errors:
                    #print("error:")
                    #print(error)
                    #messages.error(request, form.fields[field].label + ": " + error )
                    messages.error(request, error)
    else:
        form = NewUserForm()
        profile_form = ProfileForm()
    return render(request,
                  "register.html",
                  context={"form":form, 'profile_form':profile_form})


# LOGOUT
def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("main:login")

# LOGIN
@user_passes_test(is_not_logged_in, login_url='main:index')
def login_request(request):
    if (request.method == "POST"):
        form = AuthenticationForm(request, data=request.POST)
        if (form.is_valid()):

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            if (user is not None):
                login(request, user)
                messages.info(request, "You are now logged in as {}.".format(username)) #f"You are now logged in as {username}")
                return redirect("main:index")
        else:
            messages.error(request, "Invalid username or password.")


    form = AuthenticationForm()
    return render(request, 
                  "login.html",
                  {"form": form})

# GET MEI FILENAMES
@login_required(login_url='/login')
def get_filenames(request):

    app_path = os.path.dirname(os.path.abspath(__file__))
    static_path = static('mei/')
    path = app_path + static_path
    song_names = os.listdir(path)
    song_names.sort()
    data = {
        'file_list': song_names
    }
    return JsonResponse(data)

# GET RESULT FILENAMES
@login_required(login_url='/login')
def get_result_filenames(request):

    app_path = os.path.dirname(os.path.abspath(__file__))
    static_path = static('result/')
    path = app_path + static_path
    song_names = os.listdir(path)
    data = {
        'file_list': song_names
    }
    return JsonResponse(data)

# POST XML
@login_required(login_url='/login')
def post_xml_patterns(request):
    
    current_user = request.user
    user_id = current_user.id
    which_song = request.POST.get('which_song', None)
    xml_string_pattern = request.POST.get('xml_string_pattern', None)
    pattern_rank = request.POST.get('pattern_rank', None)
    pattern_timestamp = request.POST.get('pattern_timestamp', None)
    print()
    print("user id is: ", user_id)
    print("song id is: ", which_song)
    print(xml_string_pattern)
    print()
    pat = Pattern(user_id=user_id, song_id=which_song, xml_file=xml_string_pattern, pattern_rank=pattern_rank, pattern_timestamp=pattern_timestamp)
    pat.save()

    data = {
        'code': "200",
        'user': user_id,
        'which_song': which_song
    }
    return JsonResponse(data)

# email verification
@user_passes_test(is_not_logged_in, login_url='main:index')
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.info(request, "Thank you for your email confirmation.")#f"Thank you for your email confirmation")
        messages.info(request, "You are now logged in as {}.".format(user.username))
        return redirect('main:index')
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')