# Generated by Django 2.2.1 on 2019-07-16 00:03

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Pattern',
            fields=[
                ('pattern_id', models.AutoField(default=0, primary_key=True, serialize=False)),
                ('user_id', models.IntegerField(null=True)),
                ('song_id', models.IntegerField(null=True)),
                ('xml_file', models.TextField(null=True)),
                ('pattern_rank', models.IntegerField(null=True)),
                ('pattern_timestamp', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tutorial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tutorial_title', models.CharField(max_length=200)),
                ('tutorial_content', models.TextField()),
                ('tutorial_published', models.DateTimeField(default=datetime.datetime(2019, 7, 16, 2, 3, 30, 719111), verbose_name='date published')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('age', models.PositiveIntegerField(null=True)),
                ('instrument', models.CharField(max_length=200)),
                ('instrument_years', models.PositiveIntegerField(null=True)),
                ('theory_years', models.PositiveIntegerField(null=True)),
                ('country', models.CharField(max_length=200)),
                ('university', models.CharField(max_length=200)),
                ('programme', models.CharField(max_length=200)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
