var vrvToolkit = new verovio.toolkit();
var page = 1;
var zoom = 60;
var pageHeight = 2970;
var pageWidth = 2100;


////////////////////////////////////////////////
/* A variable for storing the highlighted ids */
////////////////////////////////////////////////
var ids = [];
var note_start_ids = [];
var note_end_ids = [];
var isPlaying = false;
var loaded_filenames = false;
var filenames = [];
//var filenames = ["001_Beethoven_Sonata_no.29_op.106.xml","Binchois.xml","test.xml", "002_Beethoven_StringQuartet_op.18_no.2.xml"];
//loaded_filenames = true
var patterns = [];

function Pattern(id, time_start, time_end, note_start_id, note_end_id, staff, rank){
    this.id = id;
    this.time_start = time_start;
    this.time_end = time_end;
    this.note_start_id = note_start_id;
    this.note_end_id = note_end_id;
    this.staff = staff;
    this.rank = rank;
}

var songID = 0;
var clicked = false;
var click_mode = 3; // interaction with notes -> selecting patterns
var patterns_start = [];
var patterns_end = [];
var patterns_staff = [];
var patterns_rank = [];
var patterns_id = [];
var patterns_timestamps = [];



var next_pattern_id = 0;
var limit_of_songs = 0;

var task_or_results = 0;

function ask_for_filenames(){
    
    if(task_or_results == 0){
        return ask_for_mei_filenames();
    }
    else{
        return ask_for_result_filenames();
    }
}

function ask_for_mei_filenames(){
    return $.ajax({
        url: '/ajax/get_filenames/',
        datatype: 'json',
        success: function(result) {
            console.log("Call to get_mei_filenames succeded: ");
            filenames = result.file_list;
            console.log(result.file_list)
        },
        error: function(req, err){ 
            console.log("Call to get_mei_filenames failed; " + err);
        },
    });
    // PHP version
    //$.ajax({
    //    type: 'POST',
    //    url: 'main/static/php/get_mei_filenames.php',
    //    dataType: 'json',
    //    
    //    success: function(result) {
    //        console.log("Call to get_mei_filenames succeded: ");
    //        filenames = result;
    //    },
    //    error: function(req, err){ 
    //        console.log("Call to get_mei_filenames failed; " + err);
    //    },
    //});
    //console.log("END OF AJAX: "+ filenames);
    
}

function ask_for_result_filenames(){
    return $.ajax({
        type: 'POST',
        url: '/static/php/get_result_filenames.php',
        dataType: 'json',
        
        success: function(result) {
            console.log("Call to get_result_filenames succeded: ");
            filenames = result;
        },
        error: function(req, err){ 
            console.log("Call to get_result_filenames failed; " + err);
        },
    });   
}

function setOptions() {
    pageHeight = $(document).height() * 70 / zoom ;
    pageWidth = $(document).width() * 90 / zoom ;
    // set spacing between system (vrstami staff)
    options = {
    pageHeight: pageHeight,
    pageWidth: pageWidth,
                scale: zoom,
                adjustPageHeight: true,
    spacingSystem: 6,
    //defaultTopMargin: 6.0

            };
    vrvToolkit.setOptions(options);
    //console.log("Hei: " + pageHeight);
    //console.log("Wid:" + pageWidth);
}

function cover_next_and_previous_buttons(){
    
    if(page == 1){
        $("#button_previous_page").hide();
        $("#cover_button_previous_page").show();
    }
    else{
        $("#button_previous_page").show();
        $("#cover_button_previous_page").hide();
    }
    //console.log("PAGE COUNT: " + vrvToolkit.getPageCount()+ " ");
    if(vrvToolkit.getPageCount() == 1 || page == vrvToolkit.getPageCount()){ 
        $("#button_next_page").hide();
        $("#cover_button_next_page").show();         
    }
    else{
        //button_next_page.style.backgroundColor='rgb(11, 19, 87)';
        $("#button_next_page").show();
        $("#cover_button_next_page").hide();  
    }
    //console.log(document.getElementById("page_number").innerHTML);
    document.getElementById("page_number").innerHTML = page + "/" + vrvToolkit.getPageCount();
    document.getElementById("task_number").innerHTML = (songID + 1 ) + "/" + limit_of_songs; 
    //console.log(document.getElementById("page_number").innerHTML);
}   

function loadData(data) {
    setOptions();
    
    vrvToolkit.loadData(data);
    cover_next_and_previous_buttons();
    page = 1;
    
    loadPage();
}

function show_warning(which_warning){

    // Get the modal
    var modal_warning;
    // Get the <span> element that closes the modal
    var span;

    //warning for incorrect marking
    if(which_warning == 0){
        modal_warning  = document.getElementById('overlay_warning');
        span = document.getElementsByClassName("close_warning")[0];
        console.warn('Warning for wrong click.');
    }

    //warning for incorrect submission
    else if(which_warning == 1){
        modal_warning  = document.getElementById('submission_warning');
        span = document.getElementsByClassName("close_warning")[1];
        console.warn('Warning for unfinished pattern.');
    }                                
    
    // submission confirmation 
    else if(which_warning == 2){
        modal_warning = document.getElementById('submission_confirmation');
        span = document.getElementsByClassName("close_warning")[2];
        console.warn('Request for pattern confirmation.');
    }
    // The end, finito, user has finished all tasks
    else if(which_warning == 3){
        modal_warning = document.getElementById('modal_the_end');
        span = document.getElementsByClassName("close_warning")[3];

        console.warn('Thank you for participating.');
    }
    // When the user clicks the button, open the modal 
    //modal_warning.style.display = "block";
    $(modal_warning).modal('open');
    // When the user clicks on <span> (x), close the modal
    //span.onclick = function() {
    //    $(modal_warning).modal('close');
    //    //modal_warning.style.display = "none";
    //}
    // When the user clicks anywhere outside of the modal, close it
    //window.onclick = function(event) {
    //    if (event.target == modal_warning) {
    //        modal_warning.style.display = "none";
    //    }
    //}

}

var time_starter = 0;
var staff_id = "";
var staff_num = -1;
var svg_start_note_coordinates;
var svg_system_line_start;
var svg_system_line_end;

var offset_for_rank = 0;
var offset_for_close = 0;
var general_offset = 0;

var colour_played_note = "#15adea";//"#32cd32";

function loadPage() {
    
    svg = vrvToolkit.renderToSVG(page, {});
    //svg.setAttribute("pointer-events","bounding-box");
    //console.log(svg);
    $("#svg_output").html(svg);

    $("#svg_output")[0].childNodes[0].setAttribute("pointer-events","bounding-box");
    $("#svg_output")[0].childNodes[0].setAttribute("class","svg_music");
    
    //$("#svg_output")[0].childNodes[0].setAttribute("overflow-y","auto");
    svg_all = $("#svg_output")[0].childNodes[0];
    svg_all.style.webkitUserSelect = "none";
    svg_all.style.webkitTouchCallout = "none";
    svg_all.style.MozUserSelect = "none";
    svg_all.style.msUserSelect = "none";



    limit_of_songs = filenames.length;
    // LIMIT HERE
    if ( task_or_results == 0){
        limit_of_songs = 6;
    }


    ////////////////////////////////////////
    /* Bind a on click event to each note */
    ////////////////////////////////////////
    
            
    cover_next_and_previous_buttons();
    // when moving pages, check for any patterns that have been found and colour them   
    reload_patterns(1);
    $(".note").click(function() {
        
        var id = $(this).attr("id");
        
        // depends on the click mode
        // colour the clicked note BLUE
        if(click_mode == 0){
            console.log(Date.now())
            console.log(this);
            console.log(vrvToolkit.getElementAttr(id));
            $(this).attr("fill", "#00e").attr("stroke", "#00e");
            var time = vrvToolkit.getTimeForElement( $(this).attr("id"));
            //var tmp_svg_element = $("#svg_output")[0].childNodes[0].childNodes[7].childNodes[1].childNodes[5].childNodes[1].childNodes[3].childNodes[15].childNodes[1].childNodes[5];//.childNodes[3];
            //                                                                                                       measure       staff     div            beam          note
            //console.log(tmp_svg_element);
            //var tmp_obj = tmp_svg_element.getBoundingClientRect();
            
            //console.log(tmp_obj);
            console.log("STARTING TIME : " + time);
            //<g class="note" id="note-0000001187366817" fill="#00e" stroke="#00e">

            var svgn = $("#svg_output")[0].childNodes[0];
            var svg_origin_coordinates = svgn.getBoundingClientRect();

            //console.log( svgn.getBoundingClientRect());
            var svg_clicked_note = svgn.getElementById(id);
            console.log(svg_clicked_note);
            //console.log(svg_clicked_note.getBoundingClientRect());
            var svg_note_coordinates = svg_clicked_note.getBoundingClientRect();

            var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
            rect.setAttributeNS(null, 'x', svg_note_coordinates.x - svg_origin_coordinates.x);//199.25);
            rect.setAttributeNS(null, 'y', svg_note_coordinates.y - svg_origin_coordinates.y);//350.1);
            rect.setAttributeNS(null, 'height', 100);// svg_note_coordinates.height);//67.78);
            rect.setAttributeNS(null, 'width', 40);// 31.11);
            rect.setAttributeNS(null, 'stroke', '#c00');
            rect.setAttributeNS(null, 'fill', 'white');
            svg_container = $("#svg_output")[0].childNodes[0];
            svg_container.insertBefore(rect, svg_container.firstChild);

        }

        // allow multiple notes to be selected
        else if(click_mode == 1){
            // first click, marks the start of the pattern
            if( clicked == false){
                var note_id = $(this).attr("id");
                var time_of_initial_note = vrvToolkit.getTimeForElement(note_id);
                
                // check if user wants to delete pattern 
                // user needs to click on the start of the pattern to delete the whole thing
                var index_of_pattern = patterns_start.indexOf(time_of_initial_note);
                
                if(index_of_pattern != -1){
                    // delete pattern from array and colour the notes black
                    delete_pattern(index_of_pattern);        
                }

                else{
                    //var id_starter; 
                    $(".note").each(function(i) {
                        var note_id = $(this).attr("id");       
                        var time_note_id = vrvToolkit.getTimeForElement(note_id);
                        
                        // colour the START note BLUE
                        if (time_note_id == time_of_initial_note){
                            $(this).attr("fill", "#00e").attr("stroke", "#00e");
                            //id_starter = note_id;
                        }
                    }); 
                    //$(this).attr("fill", "#00e").attr("stroke", "#00e");
                    clicked = true;
                    time_starter = vrvToolkit.getTimeForElement(id);
                    
                    //console.log("Start of pattern: " + time_starter);
                    patterns_start.push(time_starter);
                    patterns_rank.push(1);
                    patterns_staff.push(-1);
                    patterns_id.push(next_pattern_id);
                    note_start_ids.push(id);
                    //console.log( "STARTER ID: " +id);
                }
                
            }
            // second click, marks the end of the pattern, and colours the whole pattern
            else if(clicked == true){
                
                
                var time_ender = vrvToolkit.getTimeForElement(id);
                // check if user wants to delete pattern 
                // user needs to click on the start of the pattern to delete the whole thing
                var index_of_pattern = patterns_start.indexOf(time_ender);
                if(index_of_pattern != -1){
                    // check if we clicked on the currently started pattern
                    // delete it and clicked = true, allow to finished before started pattern
                    // if we clicked on a previously finished pattern delete that, and clicked = false, allow to start new pattern
                    if(index_of_pattern == patterns_end.length ){
                        clicked=false;
                    }
                    // delete pattern from array and colour the notes black
                    delete_pattern(index_of_pattern);
                }
                
                //check if ending time is before the first click, thus proccing a warning
                else if(time_ender < time_starter ){
                    // show warning message
                    show_warning(0);
                }

                else{
                    //console.log("End of pattern: " + time_ender);
                    patterns_end.push(time_ender);
                    note_end_ids.push(id);
                    $(".note").each(function(i) {
                        var note_id = $(this).attr("id");

                        var time_note_id = vrvToolkit.getTimeForElement(note_id);
                        //console.log("START: "+ time_starter + "   ENDER: "+ time_ender);
                        // colour the pattern notes RED
                        if (time_note_id > time_starter && time_note_id < time_ender ){
                            //console.log("DONE");
                            $(this).attr("fill", "#c00").attr("stroke", "#c00");
                        }
                        // colour the END note PURPLE
                        else if (time_note_id == time_ender){
                            $(this).attr("fill", "#8A2BE2").attr("stroke", "#8A2BE2");
                        }
                        clicked = false;
                    }); 
                }
            }          
        }  
        

        // click mode 3
        // same as click mode 2 + insert rectange in svg
        else if(click_mode == 3){
            var svgn = $("#svg_output")[0].childNodes[0];
            var svg_origin_coordinates = svgn.getBoundingClientRect();

            // first click, marks the start of the pattern
            if( clicked == false){
                var note_id = $(this).attr("id");

                //console.log("on page" + vrvToolkit.getPageWithElement(note_id));
                // SVG coordinates used for SVG
                var svg_clicked_note = svgn.getElementById(id);
                //console.log(svg_clicked_note);
                svg_start_note_coordinates = svg_clicked_note.getBoundingClientRect();
                //console.log(svg_start_note_coordinates);
                var tmp_svg_note = svg_clicked_note;
                while(tmp_svg_note.attributes.class.nodeValue != "system"){
                    tmp_svg_note = tmp_svg_note.parentNode;
                }
                //console.log(tmp_svg_note);
                //console.log(tmp_svg_note.attributes.id.nodeValue);
                svg_system_line_start = tmp_svg_note;
               

                //var note_staff_number = $(this).attr("staff");
                var time_of_initial_note = vrvToolkit.getTimeForElement(note_id);
                var parent_of_note = $(this)[0];
                parent_of_note = parent_of_note.parentNode;
 
                // FIND which staff
                // if parent is a chord, go one layer higher to layer, and then to staff
                while(parent_of_note.attributes.class.nodeValue != "staff"){
                    parent_of_note = parent_of_note.parentNode;
                }
                staff_id = parent_of_note.attributes.id.nodeValue;
                
                var children_of_measure = parent_of_note.parentNode.childNodes;
                var staff_tmp = 0;
                for (var i = 0; i < children_of_measure.length; i++){
                    if(children_of_measure[i].nodeName == "g"){
                        if( children_of_measure[i].attributes.class.nodeValue == "staff"){
                            staff_tmp = staff_tmp + 1;
                            if( children_of_measure[i].attributes.id.nodeValue == staff_id){
                                break;
                            }    
                        }
                    }
                }
                staff_num = staff_tmp;

                // check if user wants to delete pattern 
                // user needs to click on the start of the pattern to delete the whole thing
                var index_of_pattern = patterns_start.indexOf(time_of_initial_note);
                
                offset_for_rank = 0;
                if(index_of_pattern != -1){

                    var amount_of_same_starts = 0;
                    for (var i = 0; i < patterns_start.length; i++  ){
                        if( time_of_initial_note == patterns_start[i]){
                            amount_of_same_starts = amount_of_same_starts + 1;
                        }
                    }
                    offset_for_rank = amount_of_same_starts;
                    
                    // delete pattern from array and colour the notes black
                    //delete_pattern(patterns_id[index_of_pattern], 1);
                    
                }
                //else{ 
                    $(".note").each(function(i) {
                        var note_id = $(this).attr("id");       
                        var time_note_id = vrvToolkit.getTimeForElement(note_id);
                        
                        // colour the START note BLUE
                        if (time_note_id == time_of_initial_note){  
                            var tmp_node = $(this)[0].parentNode;
                    
                            while(tmp_node.attributes.class.nodeValue != "staff"){
                                tmp_node = tmp_node.parentNode;
                            }
                            var tmp_staff_id = tmp_node.attributes.id.nodeValue;
                            var children_of_measure = tmp_node.parentNode.childNodes;
                            var staff_tmp_num = 0;
                            for (var i = 0; i < children_of_measure.length; i++){
                                if(children_of_measure[i].nodeName == "g"){
                                    if( children_of_measure[i].attributes.class.nodeValue == "staff"){
                                        staff_tmp_num = staff_tmp_num + 1;
                                        if( children_of_measure[i].attributes.id.nodeValue == tmp_staff_id){
                                            break;
                                        }
                                    }
                                }
                            }
                            if(staff_tmp_num == staff_num){              
                                $(this).attr("fill", "#00e").attr("stroke", "#00e");
                            }
                        }
                    }); 
                    clicked = true;
                    time_starter = vrvToolkit.getTimeForElement(id);
                    patterns_start.push(time_starter);
                    patterns_staff.push(staff_num);
                    patterns_id.push(next_pattern_id);
                    note_start_ids.push(id);
                    patterns_rank.push(1);
                //}
                
            }
            // second click, marks the end of the pattern, and colours the whole pattern
            else if(clicked == true){
                
                var time_ender = vrvToolkit.getTimeForElement(id);
                
                // draw svg rectangle around the pattern
                var svg_clicked_note = svgn.getElementById(id);
                //console.log("on page" + vrvToolkit.getPageWithElement(id));
                var tmp_svg_note = svg_clicked_note;

                while(tmp_svg_note.attributes.class.nodeValue != "system"){
                    tmp_svg_note = tmp_svg_note.parentNode;
                }
                //console.log(tmp_svg_note);
                svg_system_line_end = tmp_svg_note;
                
                // check if user wants to delete pattern 
                // user needs to click on the start of the pattern to delete the whole thing
                
                // QUICK FIX
                
                //var index_of_pattern = patterns_start.indexOf(time_ender);
                
                if( time_ender == patterns_start[patterns_start.length-1] ){// index_of_pattern != -1){
                    console.log("Delete previous start");
                    
                    delete_pattern(patterns_id[patterns_id.length-1], 0);
                    clicked=false;
                    
                    // check if we clicked on the currently started pattern
                    // delete it and clicked = true, allow to finished before started pattern
                    // if we clicked on a previously finished pattern delete that, and clicked = false, allow to start new pattern
                    //if(index_of_pattern == patterns_end.length ){
                    //    
                    //}
                    //console.log("hey");
                    // delete pattern from array and colour the notes black
                    
                    
                }
                

                

                //check if ending time is before the first click, thus proccing a warning
                else if(time_ender < time_starter ){
                    // show warning message
                    show_warning(0);
                    return;
                }
                else{
                    // check if the closes will overlap with other patterns, try moving them
                    var are_there_more_ends = patterns_end.indexOf(time_ender);
                    offset_for_close = 0;
                    
                    if(are_there_more_ends != -1){

                        var amount_of_same_ends = 0;
                        for (var i = 0; i < patterns_end.length; i++  ){
                            if( time_ender == patterns_end[i]){
                                amount_of_same_ends = amount_of_same_ends + 1;
                            }
                        }

                        offset_for_close = amount_of_same_ends;
                        // delete pattern from array and colour the notes black
                        //delete_pattern(patterns_id[index_of_pattern], 1);


                    }
                    // TODO
                    // check if close and rank will overlap somewhere
                                        
                    if( offset_for_close > offset_for_rank){
                        general_offset = offset_for_close;
                        
                    }
                    else{
                        
                        general_offset = offset_for_rank;
                    }
                    
                    //console.log("End of pattern: " + time_ender);
                    patterns_end.push(time_ender);
                    note_end_ids.push(id);
                    // save timestamp when the pattern was annotated
                    
                    patterns_timestamps.push( Math.floor(Date.now() / 1000) );

                    var svg_end_note_coordinates = svg_clicked_note.getBoundingClientRect();
                    draw_svg_rect_pattern(svg_system_line_start, svg_system_line_end, svg_start_note_coordinates, svg_end_note_coordinates,svg_origin_coordinates, next_pattern_id);
                    
                    $(".note").each(function(i) {
                        var note_id = $(this).attr("id");
                    
                        var time_note_id = vrvToolkit.getTimeForElement(note_id);

                        // colour the pattern notes RED
                        if (time_note_id > time_starter && time_note_id < time_ender ){
                            var tmp_node = $(this)[0].parentNode;
                    
                            while(tmp_node.attributes.class.nodeValue != "staff"){
                                tmp_node = tmp_node.parentNode;
                            }
                            var tmp_staff_id = tmp_node.attributes.id.nodeValue;
                            var children_of_measure = tmp_node.parentNode.childNodes;
                            var staff_tmp_num = 0;
                            for (var i = 0; i < children_of_measure.length; i++){
                                if(children_of_measure[i].nodeName == "g"){
                                    if( children_of_measure[i].attributes.class.nodeValue == "staff"){
                                        staff_tmp_num = staff_tmp_num + 1;
                                        if( children_of_measure[i].attributes.id.nodeValue == tmp_staff_id){
                                            break;
                                        }
                                    
                                    }
                                }
                            }
                            if(staff_tmp_num == staff_num){
                                //console.log("correct");
                                // only colour the element, if it was not coloured before // avoid problems with covering up previous patterns
                                //    console.log( $(this)[0].attributes.getNamedItem('fill'));
                                //console.log( $(this)[0].attributes.getNamedItem('fill'));
                                if( $(this)[0].attributes.getNamedItem('fill') == null || $(this)[0].attributes.getNamedItem('fill').nodeValue == "#000"){
                                    $(this).attr("fill", "#c00").attr("stroke", "#c00");
                                }
                                
                            }
                            
                        }
                        // colour the END note PURPLE
                        else if (time_note_id == time_ender){
                            //console.log("DONE");
                            var tmp_node = $(this)[0].parentNode;
                    
                            while(tmp_node.attributes.class.nodeValue != "staff"){
                                tmp_node = tmp_node.parentNode;
                            }
                            var tmp_staff_id = tmp_node.attributes.id.nodeValue;
                            var children_of_measure = tmp_node.parentNode.childNodes;
                            //console.log(children_of_measure);
                            var staff_tmp_num = 0;
                            for (var i = 0; i < children_of_measure.length; i++){
                                if(children_of_measure[i].nodeName == "g"){
                                    if( children_of_measure[i].attributes.class.nodeValue == "staff"){
                                        staff_tmp_num = staff_tmp_num + 1;
                                        if( children_of_measure[i].attributes.id.nodeValue == tmp_staff_id){
                                            //console.log(staff_id);
                                            break;
                                        }
                                    
                                    }
                                }
                            }
                            if(staff_tmp_num == staff_num){
                                // QUICK FIX
                                //$(this).attr("fill", "#c00").attr("stroke", "#c00");
                                $(this).attr("fill", "#8A2BE2").attr("stroke", "#8A2BE2");
                            }
                            
                        }
                
                        clicked = false;
                        
                    }); 
                    
                }
                plus_or_minus_height = plus_or_minus_height * -1;
                next_pattern_id = next_pattern_id + 1;
                colour_index = colour_index + 1;
                if( colour_index >= colours.length){
                    colour_index = 0;
                }
            }
            reload_patterns(0);        
        } // end click_mode 3
    }); 
};

function delete_pattern(index_of_pattern, mode){
    //console.log(index_of_pattern);
    //console.log(patterns_id.indexOf(parseInt(index_of_pattern)));
    var deleting_id = patterns_id.indexOf(index_of_pattern);
    //console.log(deleting_id + " first " + index_of_pattern);
    //console.log(patterns_id[index_of_pattern]);
    if(mode == 1){
        delete_svg_rect(deleting_id); // was deleting_id
    }
    

    var time_of_end_of_pattern = patterns_end[deleting_id];
    var time_of_start_of_pattern = patterns_start[deleting_id];
    //console.log(time_of_end_of_pattern);
    if(time_of_end_of_pattern != null){
        
        //console.log("Removing: "+  patterns_start[deleting_id] + " to " +patterns_end[deleting_id]);
        // remove the pattern from both arrays
        note_start_ids.splice(deleting_id, 1);
        note_end_ids.splice(deleting_id, 1);
        patterns_start.splice(deleting_id,1);
        patterns_end.splice(deleting_id,1);
        patterns_staff.splice(deleting_id,1);
        patterns_id.splice(deleting_id,1);
        patterns_rank.splice(deleting_id, 1);
        patterns_timestamps.splice(deleting_id, 1);
        
    }
    else if(time_of_end_of_pattern == null){
        
        //console.log("Removing only start: "+  patterns_start[deleting_id] );
        // remove the pattern from both arrays
        patterns_start.splice(deleting_id,1);
        patterns_staff.splice(deleting_id,1);
        patterns_id.splice(deleting_id,1);
        note_start_ids.splice(deleting_id, 1);
        patterns_rank.splice(deleting_id, 1);
 
    }
    
    reload_patterns(0);

}   

function loadFile() {
    var folder = "";
    if(task_or_results == 0){
        folder = "static/mei/";
    }
    else{
        folder = "static/results/"
    }
    // filenames haven't been loaded, wait for them
    if(loaded_filenames == false){
        $.when(ask_for_filenames()).done(function(a1){
            console.log("Waited for filenames to load.");
            var file_name = filenames[songID];
            file = folder.concat(file_name);
            $.ajax({
                url: file,
                dataType: "text",
                success: function(data) {
                    loadData(data);
                },
                error: function(req, err){ 
                    console.log("Call failed in load one file; " + err);
                },
            });
        });
        loaded_filenames = true;
    }
    // if filenames have already been loaded, don't load them again
    else if(loaded_filenames == true){
        console.log("Not asking for mei_filenames again.");
        var file_name = filenames[songID];
        //filename = tem_mei_location;
        file = folder.concat(file_name);
        $.ajax({
            url: file,
            dataType: "text",
            success: function(data) { 
                loadData(data);
            },
            error: function(req, err){ 
                console.log("Call to load file failed; " + err);
            },
        });
    }
    
    
}

// RESETS ALL patterns, thus colouring everything black
function reset_patterns(){
    note_start_ids = [];
    note_end_ids = [];
    patterns_start = [];
    patterns_end = [];
    patterns_staff = [];
    patterns_id = [];
    patterns_timestamps = [];
    next_pattern_id = 0;
    clicked = false;

    loadPage();
}

function find_note_staff(clicked_note_object){
    // find to which staff a note belongs to
    var parent_of_note = clicked_note_object[0];
    parent_of_note = parent_of_note.parentNode;
    while(parent_of_note.attributes.class.nodeValue != "staff"){
        parent_of_note = parent_of_note.parentNode;
    }
    staff_id = parent_of_note.attributes.id.nodeValue;
    
    var children_of_measure = parent_of_note.parentNode.childNodes;
    var staff_tmp = 0;
    for (var i = 0; i < children_of_measure.length; i++){
        //console.log("hi");
        //console.log(children_of_measure);
        if(children_of_measure[i].nodeName == "g"){
            if( children_of_measure[i].attributes.class.nodeValue == "staff"){
                staff_tmp = staff_tmp + 1;
                if( children_of_measure[i].attributes.id.nodeValue == staff_id){
                    break;
                }
        
            }
        }
    }
    return staff_tmp;
}



function reload_patterns(mode){
    //console.log("hey waddup");
    //all patterns were finished
    $(".note").each(function(i) {
        $(this).attr("fill", "#000").attr("stroke", "#000");
    });
    //console.log(patterns_start.length);
    if(patterns_start.length == patterns_end.length){
        for (var i = 0; i < patterns_start.length; i++) {
            var pat_start = patterns_start[i];
            var pat_end = patterns_end[i];
            var pat_staff = patterns_staff[i];
            $(".note").each(function(i) {
                var clicked_note = $(this);
                var note_id = $(this).attr("id");                  
                var time_note_id = vrvToolkit.getTimeForElement(note_id);

                var note_staff = find_note_staff(clicked_note);
                // if note belongs to the pattern staff
                if(note_staff == pat_staff || click_mode != 2){

                    // colour the START note BLUE
                    if (time_note_id == pat_start){
                        //$(this).attr("fill", "#00e").attr("stroke", "#00e");
                        $(this).attr("fill", "#c00").attr("stroke", "#c00");
                    
                    }
                    else if (time_note_id > pat_start && time_note_id < pat_end){
                        $(this).attr("fill", "#c00").attr("stroke", "#c00");
                    }
                    else if (time_note_id == pat_end){
                        $(this).attr("fill", "#c00").attr("stroke", "#c00");
                    
                        //$(this).attr("fill", "#8A2BE2").attr("stroke", "#8A2BE2");
                    }
                }
                
                
            }); 
        } 
    }
    // a pattern was only started but not finished
    else {
        // number of patterns + 1 unfinished pattern
        var num_patterns = patterns_start.length;
        for (var i = 0; i < num_patterns; i++) {
            // last pattern was left unfinished so colour only start
            if(i == num_patterns - 1){
                var pat_start = patterns_start[i];
                $(".note").each(function(i) {
                    var note_id = $(this).attr("id");                  
                    var time_note_id = vrvToolkit.getTimeForElement(note_id);
                    var clicked_note = $(this);
                
                    var note_staff = find_note_staff(clicked_note);
                    // if note belongs to the pattern staff
                    if(note_staff == pat_staff || click_mode != 2){
                        // colour the START note BLUE
                        if (time_note_id == pat_start){
                            $(this).attr("fill", "#00e").attr("stroke", "#00e");
                        }
                    }
                    
                });
            }
            // all finished patterns
            else{
                var pat_start = patterns_start[i];
                pat_end = patterns_end[i];
                
                $(".note").each(function(i) {
                    var note_id = $(this).attr("id");                  
                    var time_note_id = vrvToolkit.getTimeForElement(note_id);
                    var clicked_note = $(this);
                    var note_staff = find_note_staff(clicked_note);
                    // if note belongs to the pattern staff
                    if(note_staff == pat_staff || click_mode != 2){
                        // colour the START note BLUE
                        if (time_note_id == pat_start){
                            $(this).attr("fill", "#c00").attr("stroke", "#c00");
                            //$(this).attr("fill", "#00e").attr("stroke", "#00e");
                        }
                        else if (time_note_id > pat_start && time_note_id < pat_end){
                            $(this).attr("fill", "#c00").attr("stroke", "#c00");
                        }
                        else if (time_note_id == pat_end){
                            $(this).attr("fill", "#c00").attr("stroke", "#c00");
                            //$(this).attr("fill", "#8A2BE2").attr("stroke", "#8A2BE2");
                        }
                    }
                    
                });
            } 
        } 

    }
    // redraw svg rectangles
    //draw_svg_rect_pattern(svg_system_line_start,svg_system_line_end, svg_clicked_note, svg_start_note_coordinates, svg_origin_coordinates ){
    if(mode == 1){
        reload_svg_rect(note_start_ids, note_end_ids);
    }
    
}

// AJAX
//output current pattern times with ajax to php file
function output_patterns_to_DB(){
    //get file ID 
    var file = filenames[songID];
    //console.log("HELP: " +file);
    var index_substring = file.substring(0,3);
    var index_of_song = parseInt(index_substring, 10);
    console.log("Submitting patterns for song: " + index_of_song);
    // go through patterns and send them to the php script
    for (var i = 0; i < patterns_start.length; i++){
        var start_pattern = patterns_start[i];
        var end_pattern = patterns_end[i];
        $.ajax({
            url: 'static/php/post_patterns.php', //This is the current doc
            type: "POST",
            data: ({which_song: index_of_song ,start_time: start_pattern, end_time: end_pattern}),
            success: function(data){
                console.log(data);  //write out success message
            }
        });   
    }
    return; 
}


function submit_and_load_next(user_confirmation){
    var modal_confirm = document.getElementById('submission_confirmation');
    $(modal_confirm).modal("close");
    //modal_confirm.style.display = "none";
    if(user_confirmation == true){
        output_xml_pattern_to_DB(patterns_start, patterns_end, note_start_ids, note_end_ids, click_mode, patterns_staff, patterns_rank, patterns_timestamps);
        //clear patterns and load next task
        note_start_ids = [];
        note_end_ids = [];
        patterns_start = [];
        patterns_end = [];
        patterns_staff = [];
        patterns_id = [];
        patterns_rank = [];
        patterns_timestamps = [];
        next_pattern_id = 0;
        
        // caused bug if player was paused and we then moved to another task
        //if(isPlaying == true){
        if(player_midi != undefined){
            // console.log("Midi player was not started, so no need to stop it");
            stopped_by = -1;
            stop_midi();
        }
        //}
        
        if(songID == limit_of_songs - 1){
            // user finished all tasks
            show_warning(3);
            //alert("Thank you for participating!")()
        }
        
        songID = (songID  + 1)% limit_of_songs;
        loadFile();

        clicked = false;
        cover_next_and_previous_buttons();
        console.log("loading next song");
    }
    else{
        return;
    }
}

// submit patterns due to click on SUBMIT button
function pressed_submit_patterns_button() {
    if(patterns_start.length != patterns_end.length){
        //var check = alert("Unfinished patterns are present in your submission.");
        // show the warning for unfinished patterns
        show_warning(1);
        return;
    }
    if(task_or_results == 1){
        var check = confirm("See next result?");
        if(check == true){
            
            note_start_ids = [];
            note_end_ids = [];
            patterns_start = [];
            patterns_end = [];
            patterns_staff = [];
            patterns_id = [];
            patterns_rank = [];
            patterns_timestamps = [];
            next_pattern_id = 0;
            
            //if(isPlaying == true){
            if(player_midi != undefined){
                stopped_by = -1;
                stop_midi();
            }
            
            if(songID == limit_of_songs - 1){
                alert("Thank you for participating!")()
            }
            
            songID = (songID  + 1)% limit_of_songs;
            loadFile();

            clicked = false;
            cover_next_and_previous_buttons();
            console.log("loading next song"); 

        }
        else{
            return;
        }
    }
    else{
        //console.log("here");
        // show confirmation modal, user gets to pick confirm or cancel
        show_warning(2);
        //var check = confirm("Submit current patterns and continue to the next task?");
        //check = false;
        //if(check == true){
        //    //output patterns with ajax to patterns.php
        //    //output_patterns_to_DB();
        //    //console.log("Currently :" + songID);
        //    output_xml_pattern_to_DB(patterns_start, patterns_end, note_start_ids, note_end_ids, click_mode, patterns_staff, patterns_rank, patterns_timestamps);
        //    //console.log("PAT_START: " + patterns_start);
        //    //console.log("PAT_END: " + patterns_end);
        //    //clear patterns and load next task
        //    note_start_ids = [];
        //    note_end_ids = [];
        //    patterns_start = [];
        //    patterns_end = [];
        //    patterns_staff = [];
        //    patterns_id = [];
        //    patterns_rank = [];
        //    patterns_timestamps = [];
        //    next_pattern_id = 0;
        //    
        //    
        //    if(isPlaying == true){
        //        //stop();
        //        stopped_by = -1;
        //        stop_midi();
        //        //midi_player.stop();
        //        //songID = (songID  + 1)% filenames.length;
        //        //
        //        //loadFile();
        //    }
        //    //else{
        //    //    songID = (songID  + 1)% filenames.length;
        //    //    loadFile();
        //    //}
        //    
        //    if(songID == limit_of_songs - 1){
        //        alert("Thank you for participating!")()
        //    }
        //    
        //    songID = (songID  + 1)% limit_of_songs;
        //    loadFile();
        //    clicked = false;
        //    cover_next_and_previous_buttons();
        //    console.log("loading next song"); 
        //}
        //else{
        //    return;
        //}
    }
}

// convert ms to normal time
function ms_to_time(s) {
    function pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
  
    return pad(mins, 1) + ':' + pad(secs, 2);
}

var player_midi;
var track_duration = 0;

////////////////////////////////////////////
/* A function that start playing the file */
////////////////////////////////////////////
function play_midi() {
    if (isPlaying == false) {
        //hides the PLAY button and replaces it with STOP button
        $("#load_midi_and_play").hide();
        var base64midi = vrvToolkit.renderToMIDI();
        var song_in_midi = 'data:audio/midi;base64,' + base64midi;
        //console.log(song_in_midi);
        $("#player").show();
        //var song_BPM = get_song_BPM(song_in_midi);
        //$("#player").midiPlayer.play();
        
        //$("#player").midiPlayer.play(song);
       
        MIDI.loader = new sketch.ui.Timer;
        //console.log(MIDI);
		MIDI.loadPlugin({
            soundfontUrl: "soundfont/",
            
			onsuccess: function() {

				/// this sets up the MIDI.Player and gets things going...
                player_midi = MIDI.Player;
                // SET BPM to the one vrv reads from XML file
                //console.log($(".tempo"));
                // TODO set this to correct value, somehow
                //player_midi.BPM = 60;
                
                //console.log(player_midi);
				player_midi.timeWarp = 1; // speed the song is played back
				player_midi.loadFile(song_in_midi, player_midi.start);
                
                // set total time
                track_duration = player_midi.endTime;
                var midiPlayer_totalTime = document.getElementById('midiPlayer_totalTime');
                midiPlayer_totalTime.innerHTML =  ms_to_time(track_duration);
                update_notes_played();

                $("#stop_midi").show();
                $("#pause_midi").show();
                isPlaying = true;
			}
        });
        

        
        //console.log("TRUE BPM: " + MIDI.Player.BPM);
        //var here = vrvToolkit.getMEI(0, 0);

        //console.log("AHEM");
        //console.log(here);
    }
}

var intervalID;

function update_notes_played(){
    intervalID = setInterval(function(){ midiUpdate(); }, 50);
}

var stopped_by = 0;
var is_paused = false;
// click on PAUSE Button
function pause_the_midi() {
    if (isPlaying == true) {
        //hides the PAUSE button and replaces it with PLAY button
        $("#pause_midi").hide();
        //pause();
        player_midi.pause();
        $("#resume_midi").show();
        isPlaying = false;
        is_paused = true;
        clearInterval(intervalID);
    }
}

// click RESUME button
function resume_midi() {
    //console.log("resume");
    if (isPlaying == false) {
        //hides the RESUME button and replaces it with PAUSE button
        $("#resume_midi").hide();
        //play();
        player_midi.resume();
        //console.log(player_midi.currentTime);
        $("#pause_midi").show();
        isPlaying = true;
        is_paused = false;
        update_notes_played();
    }
}

//////////////////////////////////////////////////////
/* Two callback functions passed to the MIDI player */
//////////////////////////////////////////////////////
var midiUpdate = function() {
    
    var cur_time = player_midi.currentTime;
    update_progress_bar(cur_time, player_midi.endTime, ms_to_time(cur_time));
    // if song is actually over
    if(cur_time > player_midi.endTime){
        //console.log("song is over");
        //console.log(player_midi.endTime);
        stop_midi();
        
    }
    //console.log(time);
    //console.log("midiUpdate");
    // time needs to - 400 for adjustment
    var vrvTime = cur_time;
    var elementsattime = vrvToolkit.getElementsAtTime(vrvTime);
    if (elementsattime.page > 0) {
        if (elementsattime.page != page) {
            page = elementsattime.page;
            loadPage();
        }
        //var colour_before_fill = $("#" + noteid).attr("fill");
        //var colour_before_stroke = $("#" + noteid).attr("stroke");       
        if ((elementsattime.notes.length > 0) && (ids != elementsattime.notes)) {
            // colour note back after being played
            ids.forEach(function(noteid) {
               //console.log($("#" + noteid).attr("fill"));
                if ($.inArray(noteid, elementsattime.notes) == -1) {
                    //$("#" + noteid).attr("fill", "#000").attr("stroke", "#000");
                    var colour_stroke_before_play = $("#" + noteid).attr("stroke");
                    // check if the colour of stroke was undefined and set it to black
                    if(colour_stroke_before_play == null){
                        $("#" + noteid).attr("fill",  "#000");
                    }
                    // if not set FILL to what STROKE is
                    else{
                        $("#" + noteid).attr("fill",  colour_stroke_before_play);
                    }
                    
                }
            });
            ids = elementsattime.notes;
            
            // colour on play
            ids.forEach(function(noteid) {               
                // if note is in elementsattime
                if ($.inArray(noteid, elementsattime.notes) != -1) {
                    //console.log( colour_before_fill + " yooo");
                    //$("#" + noteid).attr("fill", "#32CD32").attr("stroke", "#32CD32");
                    $("#" + noteid).attr("fill", colour_played_note);
                    
                }
            }); 
        }
    }
}

var stop_midi = function() {
    //console.log("HEIEI");
    // hide the STOP button and replace with PLAY button
    $("#stop_midi").hide();
    $("#pause_midi").hide();
    
    ids.forEach(function(noteid) {
        var colour_before = $("#" + noteid).attr("stroke");
        if(colour_before == null){
            $("#" + noteid).attr("fill", "#000").attr("stroke", "#000"); 
        }
        else{
            $("#" + noteid).attr("fill", colour_before); 
        }
    });
    //$("#player").hide();
    //if (player_midi != undefined){
    player_midi.stop();
    player_midi.BPM = undefined;
    //}

    update_progress_bar(0, stopped_by ,ms_to_time(0));
    stopped_by = 0;
    $("#resume_midi").hide();
    $("#load_midi_and_play").show();
    
    isPlaying = false;
    clearInterval(intervalID);
}

$(document).ready(function() {
    
    // prepare modals
    $('#overlay_warning').modal({startingTop: '24%', endingTop: '30%',});
    $('#submission_warning').modal({startingTop: '24%', endingTop: '30%',});
    $('#submission_confirmation').modal({startingTop: '24%', endingTop: '30%',});
    $('#modal_the_end').modal({startingTop: '24%', endingTop: '30%',});
    $(window).keyup(function(event){


        // key events for switching between click modes
        if(event.keyCode == 49){
            //click_mode = 0;
            //console.log("Click Mode 0");
        }
        else if(event.keyCode == 50){
            //click_mode = 1;
            //console.log("Click Mode 1");
        }
        else if(event.keyCode == 51){
            //click_mode = 2;
            //console.log("Click Mode 2");
        }
        else if(event.keyCode == 52){
            //click_mode = 3;
            //console.log("Click Mode 3");
        }
        ////////////////////////////////
        /* Key events for playing and stoping */
        ////////////////////////////////
        
        // SWITCH BETWEEN TASK AND RESULTS
        //if (event.ctrlKey && event.keyCode === 48) {
        //    task_or_results = (task_or_results+1) % 2;
        //    // switch click_mode
        //    if(task_or_results == 0){
        //        click_mode = 3;
        //    }
        //    else{
        //        click_mode = 2;  // do not allow the user to click on results
        //    }
        //    songID = 0;
        //    
        //    loaded_filenames = false;
        //    reset_patterns();
        //    loadFile();
        //    console.log("You pressed ctrl and 0");
        //}
   
        else if ( event.keyCode == 32) {
            if(isPlaying == false){
                if(is_paused == false){
                    play_midi();
                }
                else if(is_paused == true){
                    resume_midi();
                }
                
            }           
            else if(isPlaying == true){
                //console.log("pause");
                pause_the_midi();
                //stop_midi();
            }
        }
        else if( event.keyCode == 13 ){
            pressed_submit_patterns_button();
        }
        else if (event.ctrlKey && (event.keyCode == 37)) {
            firstPage();
        }
        else if (event.keyCode == 37) {
            prevPage();
        }
        else if (event.ctrlKey && (event.keyCode == 39)) {
            lastPage();
        }
        else if (event.keyCode == 39) {
            nextPage();
        }
        // see http://www.javascripter.net/faq/keycodes.htm
        else if ((event.keyCode == 107) || (event.keyCode == 187) || (event.keyCode == 61) ) {
            zoomIn();
        }
        else if ((event.keyCode == 109) || (event.keyCode == 189) || (event.keyCode == 173)) {
            zoomOut();
        }
    });
    
    $(window).resize(function(){
        //console.log("resize");
        reload_patterns(1);
        applyZoom();
    });
    
    $("#player").midiPlayer({
        color: "var(--color_two)",
        onUpdate: midiUpdate,
        onStop: stop_midi,
        width: 384,
    });
    
    loadFile();
});

