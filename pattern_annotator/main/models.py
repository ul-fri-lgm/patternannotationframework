from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    age = models.PositiveIntegerField(null=True)
    instrument = models.CharField(max_length=200)
    instrument_years = models.PositiveIntegerField(null=True)
    theory_years = models.PositiveIntegerField(null=True)
    country = models.CharField(max_length=200,)
    university = models.CharField(max_length=200)
    programme = models.CharField(max_length=200)
    
    def __str__(self):
        return "{}: {}, {}".format(self.user.username, self.instrument, self.age)
        #return f'{self.user.username}: {self.instrument}, {self.age}'

class Pattern(models.Model):
    #pattern_id = models.AutoField(primary_key=True, default=0)
    user_id = models.IntegerField(null=True)
    song_id = models.IntegerField(null=True)
    xml_file = models.TextField(null=True) # blob of text
    pattern_rank = models.IntegerField(null=True)
    pattern_timestamp = models.IntegerField(null=True)
    
    # override string method, when printed with string
    def __str__(self):
        return "Pattern({}): user({}) on song({}), with rank({})".format(self.id, self.user_id, self.song_id, self.pattern_rank)
        #return f'Pattern({self.id}): user({self.user_id}) on song({self.song_id}), with rank({self.pattern_rank})'
