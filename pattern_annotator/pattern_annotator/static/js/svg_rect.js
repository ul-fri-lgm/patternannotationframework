function draw_svg_rect_pattern(svg_system_line_start,svg_system_line_end, svg_start_note_coordinates, svg_end_note_coordinates, svg_origin_coordinates, id ){
    // pattern starts and ends in the same system row (SIMPLE)
    var start_system_coords = null;
    var end_system_coords = null;  
    var on_one_page = true;
    
    if(svg_system_line_start == null || svg_system_line_end == null ){
        on_one_page = false;
    }    

    if(svg_system_line_start != null && document.getElementById(svg_system_line_start.getAttribute('id')) != null){
        // RESET THE SYSTEM LINE in case we changed pages before clicking last note, otherwise the svg_system_line_start might not work correctly until refresh
        svg_system_line_start = document.getElementById(svg_system_line_start.getAttribute('id'));
        //console.log(svg_system_line_start);
        start_system_coords = svg_system_line_start.getBoundingClientRect();
        //console.log(svg_system_line_start);
        //console.log(start_system_coords);
    }
    if( svg_system_line_end  != null){
        end_system_coords = svg_system_line_end.getBoundingClientRect();
    }

    //var svg_end_note_coordinates = svg_clicked_note.getBoundingClientRect();
    
    var svg_group_element = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    svg_group_element.setAttributeNS(null, 'id', "pattern_"+id.toString() );

    //svg_group_element.addEventListener('click', function(){
    //    //console.log("hey: " + svg_group_element.id);
    //    delete_pattern(parseInt(svg_group_element.id));
    //});
    var svg_container = $("#svg_output")[0].childNodes[0];
    svg_container.insertBefore(svg_group_element, svg_container.firstChild);
    //svg_container.appendChild(svg_group_element);
    
    if(on_one_page && svg_system_line_end.attributes.id.nodeValue == svg_system_line_start.attributes.id.nodeValue ){
        //console.log(start_system_coords);
        draw_rect(svg_start_note_coordinates,svg_end_note_coordinates, svg_origin_coordinates, start_system_coords, svg_group_element, 0);
        //console.log("lets draw it");           
    }

    else{     


        // draw rect between starting system line and ending system line
        // go across all system lines between them and draw rects
        var tmp_system_parent;
        
        //console.log(svg_system_line_start.getAttribute('id'));
        // check if start system line is on the same page
        // if not
        if (svg_system_line_start == null || document.getElementById(svg_system_line_start.getAttribute('id')) == null){
            
            tmp_system_parent = svg_system_line_end.parentNode;
            var trigger_for_mid_pattern = true;
            for (var sys_ind = 0; sys_ind < tmp_system_parent.childNodes.length; sys_ind++){
                // if node is not just text empty
                var son_of_page = tmp_system_parent.childNodes[sys_ind];           
                if (son_of_page.attributes != null){

                    // in the last system line
                    if(son_of_page.attributes.id.nodeValue == svg_system_line_end.attributes.id.nodeValue){
                        trigger_for_mid_pattern = false;
                    }
                    else{
                        if(trigger_for_mid_pattern == true){
                            var mid_system_coords = son_of_page.getBoundingClientRect();
                            //console.log(svg_start_note_coordinates);
                            draw_rect(null, svg_end_note_coordinates,svg_origin_coordinates, mid_system_coords,svg_group_element, 2 );                
                        }
                    }           
                }       
            }
        }

        else if ( svg_system_line_end == null){
            //console.log("here");
            // finish the starting line 
            draw_rect(svg_start_note_coordinates, svg_end_note_coordinates,svg_origin_coordinates, start_system_coords, svg_group_element, 1 );
            //then add the lines inbetween AKA to the end of the page
            tmp_system_parent = svg_system_line_start.parentNode;
            var trigger_for_mid_pattern = false;
            for (var sys_ind = 0; sys_ind < tmp_system_parent.childNodes.length; sys_ind++){
                // if node is not just text empty
                var son_of_page = tmp_system_parent.childNodes[sys_ind];           
                if (son_of_page.attributes != null && son_of_page.getAttribute("class") == "system"){
                    //console.log(son_of_page.getAttribute("class"));
                    // in the last start line
                    if(son_of_page.attributes.id.nodeValue == svg_system_line_start.attributes.id.nodeValue){
                        trigger_for_mid_pattern = true;
                    }
                    else if(trigger_for_mid_pattern == true){
                        var mid_system_coords = son_of_page.getBoundingClientRect();
                        //console.log(svg_start_note_coordinates);
                        draw_rect(svg_start_note_coordinates, null,svg_origin_coordinates, mid_system_coords,svg_group_element, 2 );                
                    }
                              
                }       
            }
        }
        // if on same page draw all rects
        else{
            var tmp_system_parent = svg_system_line_start.parentNode;
            // finish the starting part, till system line end
            draw_rect(svg_start_note_coordinates, svg_end_note_coordinates,svg_origin_coordinates, start_system_coords, svg_group_element, 1 );
            
            
            //console.log(tmp_system_parent);
            var trigger_for_mid_pattern = false;
            for (var sys_ind = 0; sys_ind < tmp_system_parent.childNodes.length; sys_ind++){
                // if node is not just text empty
                var son_of_page = tmp_system_parent.childNodes[sys_ind];           
                if (son_of_page.attributes != null){
                    // pattern start system line
                    if(son_of_page.attributes.id.nodeValue == svg_system_line_start.attributes.id.nodeValue){
                        trigger_for_mid_pattern = true;
                    }
                    // in the last system line
                    else if(son_of_page.attributes.id.nodeValue == svg_system_line_end.attributes.id.nodeValue){
                        trigger_for_mid_pattern = false;
                    }
                    else{
                        if(trigger_for_mid_pattern == true){
                            var mid_system_coords = son_of_page.getBoundingClientRect();
                            //console.log(svg_start_note_coordinates);
                            draw_rect(svg_start_note_coordinates, null,svg_origin_coordinates, mid_system_coords,svg_group_element, 2 );                
                        }
                    }           
                }       
            }
        }
        
        
        if(svg_system_line_end != null){
            // draw rect at the end point, from system line start to note end
            draw_rect(svg_start_note_coordinates, svg_end_note_coordinates,svg_origin_coordinates, end_system_coords, svg_group_element, 3 )
        }
        
    }
    //plus_or_minus_height = plus_or_minus_height * -1;
    //next_pattern_id = next_pattern_id + 1;
    //colour_index = colour_index + 1;
    //if( colour_index >= colours.length){
    //    colour_index = 0;
    //}
    
    // draw the number in the upper most-left corner
    if(svg_system_line_start != null && document.getElementById(svg_system_line_start.getAttribute('id')) != null){
        //console.log("drawing number apparently");
        draw_number_rect(id, svg_start_note_coordinates, svg_origin_coordinates, start_system_coords);
    }
    // X button, upper most-right corner
    if(svg_system_line_end != null){
        draw_X_rect(id, svg_end_note_coordinates, svg_end_note_coordinates, svg_origin_coordinates, end_system_coords);
    }
   
    


    
}

function draw_number_rect(id, svg_start_note_coordinates, svg_origin_coordinates, start_system_coords){
    var svg_container = $("#svg_output")[0].childNodes[0];
    // draw the rect for numbers and number
    var svg_num_group = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    svg_num_group.setAttributeNS(null, 'id', "patNum_" + id.toString() );
    svg_num_group.addEventListener('click', function(){
        //console.log("change rank at id : " + svg_group_element.id);
        change_pattern_rank(parseInt(svg_num_group.id.split("_")[1]));
    });
    var rect_number = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    

    var top_x_coord = svg_start_note_coordinates.x - svg_origin_coordinates.x + 3;
    var top_y_coord = start_system_coords.y - svg_origin_coordinates.y - 5 - 10 *plus_or_minus_height * general_offset - 5*plus_or_minus_height;
    //var top_y_coord = start_system_coords.y - svg_origin_coordinates.y - 5 - 5 *plus_or_minus_height ;
    rect_number.setAttribute('height', 15 );
    rect_number.setAttribute('width', 15 );
    rect_number.setAttribute('x', top_x_coord);
    rect_number.setAttribute('y', top_y_coord );
    rect_number.setAttribute('rx', 3);
    rect_number.setAttribute('ry', 3 );    
    rect_number.setAttribute('stroke', "black"); //'#c00'
    rect_number.setAttribute('fill', "white");
    rect_number.setAttribute('fill-opacity', 1.0);
    rect_number.setAttribute('id', "num_rect" );
    rect_number.style.cursor = "pointer";
    svg_num_group.appendChild(rect_number);


    var svg_number = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    svg_number.setAttribute('x', top_x_coord + 4 );
    svg_number.setAttribute('y', top_y_coord + 12);
    
    svg_number.setAttribute('font-size', "13px");
    svg_number.setAttribute('stroke', "black"); //'#c00'
    svg_number.setAttribute('fill', "black");
    svg_number.setAttribute('id', "textNum_" + id.toString() );
    svg_number.style.cursor = "pointer";
    //console.log(patterns_id.indexOf(id));
    var data = document.createTextNode( patterns_rank[patterns_id.indexOf(id)]);
    svg_number.appendChild(data);
    svg_num_group.appendChild(svg_number);

    // include in main svg
    svg_container.appendChild(svg_num_group);
}

function draw_X_rect(id, svg_end_note_coordinates, svg_end_note_coordinates, svg_origin_coordinates, end_system_coords){
    var svg_container = $("#svg_output")[0].childNodes[0];
    var svg_X_rect_group = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    svg_X_rect_group.setAttributeNS(null, 'id', "delBut_" + id.toString() );
    svg_X_rect_group.addEventListener('click', function(){
        //console.log("delete pattern: " + svg_X_rect_group.id);
        delete_pattern(parseInt(svg_X_rect_group.id.split("_")[1]), 1);
    });

    var rect_delete = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    var top_x_coord = svg_end_note_coordinates.x - svg_origin_coordinates.x - 4 ;
    var top_y_coord = end_system_coords.y - svg_origin_coordinates.y - 5 - 10 *plus_or_minus_height* general_offset - 5*plus_or_minus_height;
    //var top_y_coord = end_system_coords.y - svg_origin_coordinates.y - 5 - 5 *plus_or_minus_height* offset_for_close;
    rect_delete.setAttribute('height', 15 );
    rect_delete.setAttribute('width', 15 );
    rect_delete.setAttribute('x', top_x_coord);
    rect_delete.setAttribute('y', top_y_coord );
    rect_delete.setAttribute('rx', 3);
    rect_delete.setAttribute('ry', 3 );    
    rect_delete.setAttribute('stroke', "black"); //'#c00'
    rect_delete.setAttribute('fill', "white");
    rect_delete.setAttribute('fill-opacity', 1.0);
    rect_delete.setAttribute('id', "del_rect" );
    rect_delete.style.cursor = "pointer";

    svg_X_rect_group.appendChild(rect_delete);
    
    // draw the X in the box
    var svg_line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    
    svg_line.setAttribute('x1', top_x_coord + 3 );
    svg_line.setAttribute('y1', top_y_coord + 3);
    svg_line.setAttribute('x2', top_x_coord + 12);
    svg_line.setAttribute('y2', top_y_coord + 12);
    svg_line.setAttribute('stroke', "black"); 
    svg_line.setAttribute('x_line', 1  );
    svg_line.setAttribute('stroke-width', 2  );
    svg_line.style.cursor = "pointer";
    
    svg_X_rect_group.appendChild(svg_line);

    svg_line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    svg_line.setAttribute('x1', top_x_coord + 3);
    svg_line.setAttribute('y1', top_y_coord + 12);
    svg_line.setAttribute('x2', top_x_coord + 12);
    svg_line.setAttribute('y2', top_y_coord + 3);
    svg_line.setAttribute('stroke', "black"); 
    svg_line.setAttribute('x_line', 2  );
    svg_line.setAttribute('stroke-width', 2  );
    svg_line.style.cursor = "pointer";
    
    svg_X_rect_group.appendChild(svg_line);

    svg_container.appendChild(svg_X_rect_group);
}

var plus_or_minus_height = 1;


function draw_rect(svg_start_note_coordinates, svg_end_note_coordinates, svg_origin_coordinates, current_system_coords, svg_group_element, mode){
    // mode:
    // 0  == start in same line as end
    // 1  == only starting line from start to end of line
    // 2  == between lines
    // 3  == ending line, from start of it to end note
    //console.log("draw a rectangle");
    //console.log(svg_start_note_coordinates);
    var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    var height = 0;
    var width = 0;
    var y = 0;
    // if firefox
    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if(isFirefox){
        height = current_system_coords.height * 0.9;
    }
    else{
        height = current_system_coords.height * 1.3;
    }

    
    var add_y = 10 * plus_or_minus_height * general_offset + 5*plus_or_minus_height;
    // var add_y = 5 * plus_or_minus_height * offset_for_rank;
    
    
    // simple
    if(mode == 0 || mode == 1){
        if(isFirefox){
            rect.setAttribute('x', svg_start_note_coordinates.x - svg_origin_coordinates.x) ; 
        }
        else{
            rect.setAttribute('x', svg_start_note_coordinates.x - svg_origin_coordinates.x - svg_start_note_coordinates.width/4) ;
        }
        y = current_system_coords.y - svg_origin_coordinates.y - add_y - 10
        //rect.setAttribute('y', current_system_coords.y - svg_origin_coordinates.y - add_y - 10);
    }
    // end of pattern across multiple system lines
    else if( mode == 3){
        rect.setAttribute('x', current_system_coords.x - svg_origin_coordinates.x );
        y = current_system_coords.y - svg_origin_coordinates.y - add_y - 10
        //rect.setAttribute('y', current_system_coords.y - svg_origin_coordinates.y - add_y - 10);     
    }
    
    else if(mode == 2){
        rect.setAttribute('x', current_system_coords.x - svg_origin_coordinates.x);
        y = current_system_coords.y - svg_origin_coordinates.y - add_y - 10
        //rect.setAttribute('y', current_system_coords.y - svg_origin_coordinates.y - add_y - 10); 
    }
    // set y of rect
    rect.setAttribute('y', y);


    // compute width
    // simple pattern
    if(isFirefox){ // firefox and chrome have different svg sizes apparently ...
        if(mode == 0){
            width = Math.abs(svg_start_note_coordinates.x - svg_end_note_coordinates.x ) + svg_end_note_coordinates.width/2 ;
        }
        // pattern across multiple system lines
        // start line
        else if( mode == 1){
            width = current_system_coords.width - (svg_start_note_coordinates.x - svg_origin_coordinates.x) + svg_start_note_coordinates.width  ;
        }
        // mid lines
        else if( mode == 2){
            width = current_system_coords.width;
        }
        // end line
        else if( mode == 3){
            width = Math.abs(svg_end_note_coordinates.x - current_system_coords.x ) + svg_end_note_coordinates.width/2;
        }
    }
    else{
        if(mode == 0){        
            width = Math.abs(svg_start_note_coordinates.x - svg_end_note_coordinates.x ) + svg_end_note_coordinates.width * 1.4 ;
        }
        // pattern across multiple system lines
        // start line
        else if( mode == 1){
            width = current_system_coords.width - (svg_start_note_coordinates.x - svg_origin_coordinates.x) + svg_start_note_coordinates.width * 2.7 ;
        }
        // mid lines
        else if( mode == 2){
            if(svg_start_note_coordinates == null){
                //console.log(svg_end_note_coordinates);
                width = current_system_coords.width +  svg_end_note_coordinates.width/2;
            }
            else{
                width = current_system_coords.width +  svg_start_note_coordinates.width/2;
            }
            
        }
        // end line
        else if( mode == 3){
            width = Math.abs(svg_end_note_coordinates.x - current_system_coords.x ) + svg_end_note_coordinates.width * 1.2;     
        }
    }
    // set width of rect
    rect.setAttribute( 'width', width);
    
    // check max height
    var svg_page = $("#svg_output")[0].childNodes[0];
    var max_height = parseInt(svg_page.getAttribute('height'), 10);
    // fix height
    if(height + y >  max_height ){
        height = max_height - y;
    }
    // set rect height
    rect.setAttribute('height', height);

    colour_index = svg_group_element.id.split("_")[1] % colours.length;
    //console.log(colour_index);
    // set dasharray, to determine where to draw edges, in mode 0 we do not need it
    switch(mode) {
        case 1:
            tmp_array = [width, height, width + height];
            rect.setAttribute('stroke-dasharray', tmp_array);
            break;
        case 2:
            tmp_array = [width, height];
            rect.setAttribute('stroke-dasharray', tmp_array);
            break;
        case 3:
            tmp_array = [width + height + width, height];
            rect.setAttribute( 'stroke-dasharray', tmp_array);
            break;
          // code block
    } 
    
    rect.setAttribute('stroke', colours[colour_index]); //'#c00'
    rect.setAttribute('fill', colours[colour_index]);
    rect.setAttribute('fill-opacity', 0.1);
    
    svg_group_element.insertBefore(rect, svg_group_element.firstChild);
    
}


function reload_svg_rect(note_start_ids, note_end_ids){
    var svgn = $("#svg_output")[0].childNodes[0];
    //console.log(note_start_ids);
    plus_or_minus_height = 1;
    // all patterns finished properly
    var draw_how_many;
    if(note_start_ids.length == note_end_ids.length){
        draw_how_many = note_start_ids.length;
    }
    else{
        draw_how_many = note_start_ids.length - 1;
    }
    
    for(var i = 0; i < draw_how_many; i++){
        //console.log("draw id " + i  + " " + note_start_ids.length );
        svg_start_note_coordinates = null;
        svg_end_note_coordinates = null;
        svg_system_line_end = null;
        svg_system_line_start = null;
        
        var tmp_svg_note = svgn.getElementById( note_start_ids[i] );
        
        // if the note are not on this page, break out
        if(tmp_svg_note == null){
            tmp_svg_note = svgn.getElementById( note_end_ids[i] );
            if( tmp_svg_note != null){
                var svg_end_note_coordinates = tmp_svg_note.getBoundingClientRect();
                while(tmp_svg_note.attributes.class.nodeValue != "system"){
                    tmp_svg_note = tmp_svg_note.parentNode;
                }
                var svg_system_line_end = tmp_svg_note;
                draw_svg_rect_pattern(null, svg_system_line_end, null, svg_end_note_coordinates , svgn.getBoundingClientRect(), patterns_id[i]);
                //break;
            }
        }
        else{
            var svg_start_note_coordinates = tmp_svg_note.getBoundingClientRect();
            while(tmp_svg_note.attributes.class.nodeValue != "system"){
                tmp_svg_note = tmp_svg_note.parentNode;
            }
            var svg_system_line_start = tmp_svg_note;
            
            tmp_svg_note = svgn.getElementById( note_end_ids[i] );
            if(tmp_svg_note != null){
                
                
                
                //console.log(tmp_svg_note);
                var svg_end_note_coordinates = tmp_svg_note.getBoundingClientRect();
                while(tmp_svg_note.attributes.class.nodeValue != "system"){
                    tmp_svg_note = tmp_svg_note.parentNode;
                }
            
                var svg_system_line_end = tmp_svg_note;
            }
            
        
            draw_svg_rect_pattern(svg_system_line_start, svg_system_line_end, svg_start_note_coordinates, svg_end_note_coordinates , svgn.getBoundingClientRect(), patterns_id[i]);
        }
        
        plus_or_minus_height = -1 * plus_or_minus_height;        
    }
    

    
    //console.log(tmp_svg_note);
    //console.log(tmp_svg_note.attributes.id.nodeValue);
    

}
function change_pattern_rank(id){
    var pat_id = patterns_id.indexOf(id);
    
    //console.log(pat_id);
    var new_rank = patterns_rank[pat_id] + 1;
    if(new_rank % 4 == 0){
        new_rank = 1;
    }
    patterns_rank[pat_id] = new_rank;
    // change it in svg aswell
    var svgn = $("#svg_output")[0].childNodes[0];
    var svg_pat_rank = svgn.getElementById("textNum_" + id.toString());
    svg_pat_rank.textContent = new_rank; 
    //console.log(pattern_rank.textContent);
    //console.log(patterns_rank[pat_id])

}

function delete_svg_rect(id){
    //console.log(id )
    //console.log(patterns_id);
    id = patterns_id[id];
    //console.log(id);
    var svgn = $("#svg_output")[0].childNodes[0];
    // delete the pattern rectangle
    var pattern_rect = svgn.getElementById("pattern_" + id.toString());
    //console.log(pattern_rect);
    pattern_rect.parentNode.removeChild(pattern_rect);
    
    //delete the delete button [x]
    var delete_rect = svgn.getElementById("delBut_" + id.toString());
    if(delete_rect != null){
        delete_rect.parentNode.removeChild(delete_rect);
    }

    delete_rect = svgn.getElementById("patNum_" + id.toString());
    if(delete_rect != null){
        delete_rect.parentNode.removeChild(delete_rect);
    }
    
}

var colour_index = 0;
var colours = [
    "red",
    "blue",
    "green",
    "purple",
    "orange",
    "cyan",
    "olive",
    "magenta"
]
