// script to output pattern in an xml format

function output_xml_pattern_to_DB(patterns_start, patterns_end, note_start_ids, note_end_ids, pattern_mode, patterns_staff, patterns_rank, patterns_timestamps){

    let song_id = songID;
    //console.log("Try output xml");
    // go through all submitted patterns
    //console.log(patterns_rank);
    for (let pat_id = 0; pat_id < patterns_start.length; pat_id++){
        
        // get the id for the note at which the pattern starts
        let pattern_start_id = note_start_ids[pat_id];
        let pattern_end_id = note_end_ids[pat_id];
        let staff_num = patterns_staff[pat_id];

        // USE LET to avoid pat_rank being the same in all submits
        let pat_rank = patterns_rank[pat_id];
        let pat_timestamp = patterns_timestamps[pat_id];
        //let user_identification = user_id_js;
        
        // console.log(user_identification);
        //console.log(pat_rank);
        // get the xml file for the song
        var folder = "static/mei/";
        //console.log(filenames);
        var file_name = filenames[song_id];

        //console.log(filenames[0]);
        var file = folder.concat(file_name);
        var which_xml = file;
        //console.log("......" + which_xml);
        var xhttp = new XMLHttpRequest();
        
        xhttp.onreadystatechange = function() {
            //console.log("heeree");
            //console.log("HEREERERER: "+ pat_id);
            if (this.readyState == 4 && this.status == 200) {
                
                edit_xml_to_submit(this, song_id, pat_id, pattern_start_id, pattern_end_id, patterns_start, patterns_end, staff_num, pat_rank, pat_timestamp);
                
            }
        };
        
        xhttp.open("GET", which_xml, true);
        xhttp.send();
    } 
}  

function edit_xml_to_submit(xml, song_id, pat_id, pattern_start_id, pattern_end_id, patterns_start, patterns_end, staff_num, pat_rank,pat_timestamp) {
    //console.log("here with rank" + pat_rank);

    //var startTime = new Date();
    //console.log("timer is? " + Date.now());
    //console.log("idddd?" + pat_id);
    var xmlDoc = xml.responseXML;
    var root = xmlDoc.getElementsByTagName("section")[0];   //find section, use it as the root
    //console.log("HERE:" + root.getAttribute("xml:id"));
    // find starting and ending nodes
    var starting_node;
    var ending_node;
    //var cnt = $(".beam").contents();
    //console.log("patty   " + patterns_start);
    // remove all beams for clarity
    var all_notes = xmlDoc.getElementsByTagName("note");
    
    for (var n = 0; n < all_notes.length; n++){
        var temporary_checker = all_notes[n];
        if(temporary_checker.getAttribute("xml:id") == pattern_start_id){
            starting_node = temporary_checker;
            //console.log("I FOUND STARTING: "+ vrvToolkit.getTimeForElement(starting_node.getAttribute("xml:id")));
        }
        else if(temporary_checker.getAttribute("xml:id") == pattern_end_id){
            ending_node = temporary_checker;
        }

    } 
    
    var temp_measure;
    var start_measure;
    var end_measure;
    
    // find starting measure and ending measure
    for (var s_e = 0; s_e < 2; s_e++ ){
        var temp_nodes;
        if(s_e == 0){
            //console.log("temp_node == start_node");
            temp_nodes = starting_node;
        }
        else if(s_e == 1){
            //console.log("temp_node == end_node");
            temp_nodes = ending_node;
        }
        while(true){
            //console.log("are we clear :_?!" + starting_node.getAttribute("xml:id"));
            //console.log("went up");
            if( temp_nodes.nodeType == 1 && temp_nodes.tagName == "measure"){
                temp_measure = parseInt(temp_nodes.getAttribute("n"));
                //console.log(temp_measure);
                if(s_e == 0){
                    start_measure = temp_measure;
                }
                else{
                    end_measure = temp_measure;
                }
                break;
            }
            temp_nodes = temp_nodes.parentNode;
            //console.log("went up");
        }
    }
    
   // delete redundant measures
    var nm_measure_nodes = root.childNodes.length;
    for (var i = 0; i < nm_measure_nodes; i++){
        var node = root.childNodes[i];
        // check if node is an ELEMENT node 
        if (node.nodeType == 1 ) {    
            // check if node is the measure node           
            if(node.tagName == "measure"){
                
                var measure_number = parseInt(node.getAttribute("n"));    // get measure, n number
                if(measure_number < start_measure || measure_number > end_measure ){
                    root.removeChild(node);
                    //console.log("REMOVED MEASURE: " + measure_number);
                    nm_measure_nodes--;
                    i--;
                }
                //else if(measure_number < end_measure){
                    //console.log( measure_number + " is smaller than " + end_measure);
                //}
            }   
        }
        
    }
    // deleted redundant measures 

    // if in seperate staff mode
    if( staff_num != -1){

        // delete redundant staves
        //console.log(xmlDoc.getElementsByTagName("staff"));
        var nm_staff_nodes = xmlDoc.getElementsByTagName("staff").length;
        for (var i = 0; i < nm_staff_nodes; i++){
            var node = xmlDoc.getElementsByTagName("staff")[i];

            var staff_number = parseInt(node.getAttribute("n"));    // get measure, n number
            // if we are not keeping that staff
            if(staff_number != staff_num ){
                node.parentNode.removeChild(node);
                //console.log("REMOVED STAFF: " + staff_number);
                nm_staff_nodes--;
                i--;
            }
        }

        // delete dependancy on staff_def, for measure signatures
        //console.log(xmlDoc.getElementsByTagName("staffDef"));
        var nm_defstaff_nodes = xmlDoc.getElementsByTagName("staffDef").length;
        for (var i = 0; i < nm_defstaff_nodes; i++){
            var node = xmlDoc.getElementsByTagName("staffDef")[i];

            var staff_number = parseInt(node.getAttribute("n"));    // get measure, n number
            // if we are not keeping that staff
            if(staff_number != staff_num ){
                node.parentNode.removeChild(node);
                //console.log("REMOVED DEFSTAFF: " + staff_number);
                nm_defstaff_nodes--;
                i--;
            }
        }
    }


    // remove beams
    var b_number = xmlDoc.getElementsByTagName("beam").length;
    for (var b = 0; b < b_number; b++){
        var children_back = [];
        //console.log("beams: " + b);
        var tmp_beam = xmlDoc.getElementsByTagName("beam")[b];
        var tmp_beam_children = tmp_beam.childNodes.length;
        for(var b_c = 0; b_c < tmp_beam_children; b_c++){
            var beam_child = tmp_beam.childNodes[b_c];
            // if child is element, extract and put in 
            //if(beam_child.nodeType == 1){
            children_back.push( beam_child);
           // }
        }
        for(var a = 0; a < children_back.length; a++ ){
            tmp_beam.parentNode.insertBefore(children_back[a], tmp_beam);
        }
        //tmp_beam.parentNode.removeChild(tmp_beam);
        //b= b -1;
    }
    for (var b = 0; b < b_number; b++){
        var tmp_beam = xmlDoc.getElementsByTagName("beam")[b];
        tmp_beam.parentNode.removeChild(tmp_beam);
        b--;
        b_number--;
        //console.log("removed: ");
    }

    //console.log("After deleting measures : "+ outTime);
    // delete notes before the start and the end, in the same measure
    for (var s_e = 0; s_e < 2; s_e++){

        var node_tmp;
        // starting measure node
        var starter_measure_node = xmlDoc.getElementsByTagName("measure")[0];
        var ending_measure_node = xmlDoc.getElementsByTagName("measure")[end_measure - start_measure];
        //console.log("here lies: " + starter_measure_node.getAttribute("n")  + "    toooo: " + ending_measure_node.getAttribute("n"));
        // delete redundant notes at the starting measure 
        //console.log("ya boi here is the: " + layer_ja.getAttribute("xml:id"));

        // starting measure
        if(s_e == 0){
            var starting_layers = starter_measure_node.getElementsByTagName("layer");
            // go across all layers of the starting measure and delete redundant notes / chords 
            for (var lyrs = 0; lyrs < starting_layers.length; lyrs++){
                //node_tmp = starting_node;
                var node_start_layer = starting_layers[lyrs];

                // go through layer children
                for (var k = 0; k < node_start_layer.childNodes.length; k++){
                    var checker_node = node_start_layer.childNodes[ k ];
                    //console.log("ahem? ");
                    if(checker_node.nodeType == 1){
                        //console.log("check node  " + k);

                        // if node is a chord, find one of its notes, and continue
                        if(checker_node.tagName == "chord"){
                            for (var im = 0; im < checker_node.childNodes.length; im++ ){

                                if(checker_node.childNodes[im].nodeType == 1){
                                    checker_node = checker_node.childNodes[im];
                                    break;
                                }
                            }

                        }

                        var checker_id = checker_node.getAttribute("xml:id");
                        var checker_time = vrvToolkit.getTimeForElement(checker_id);
                        //console.log(" time for the check :  "+ checker_id  + "......" + checker_time);
                        //console.log("pattern starts at " + patterns_start[pat_id]);
                        if(checker_time < patterns_start[pat_id] || checker_node.tagName == "rest"){
                            
                            // replace whole chord
                            if(checker_node.parentNode.tagName == "chord"){
                                var checker_chord = checker_node.parentNode;
                                var previous_dur = checker_chord.getAttribute("dur");
                                // Create a rest (pause) element, with attribute dur = deleted elements dur
                                // and namespace = the parent namespace
                                var new_node = xmlDoc.createElementNS("http://www.music-encoding.org/ns/mei","rest" );
                                new_node.setAttribute("dur", previous_dur );
                                // if the chord is dotted, add dots to the rest aswell
                                if(checker_chord.getAttribute("dots") != null){
                                    var num_dots = checker_chord.getAttribute("dots");
                                    new_node.setAttribute("dots", num_dots );                             
                                }

                                node_start_layer.replaceChild(new_node, checker_chord);
                            }
                            // replace note
                            else if(checker_node.tagName == "note"){

                                var previous_dur = checker_node.getAttribute("dur");
                                // Create a rest (pause) element, with attribute dur = deleted elements dur
                                // and namespace = the parent namespace
                                var new_node = xmlDoc.createElementNS("http://www.music-encoding.org/ns/mei","rest" );
                                new_node.setAttribute("dur", previous_dur );
                                //node_start_layer.insertBefore(new_node,checker_node);
                                
                                // if the note is dotted, add dots to the rest aswell
                                if(checker_node.getAttribute("dots") != null){
                                    var num_dots = checker_node.getAttribute("dots");
                                    new_node.setAttribute("dots", num_dots );                             
                                }
                                // DOES NOT WORK FOR SOME ODD REASON
                                // if the note is dotted add more rests, to fill up the space
                                //if(checker_node.getAttribute("dots") != null){
                                //    if(checker_node.getAttribute("dots") == 1){
                                //        var dot_node = xmlDoc.createElementNS("http://www.music-encoding.org/ns/mei","rest" );
                                //        var previous_dur_dot = previous_dur * 2;
                                //        dot_node.setAttribute("dur", previous_dur_dot );
                                //        node_start_layer.replaceChild(dot_node, checker_node);
                                //    }
                                //}
                                node_start_layer.replaceChild(new_node,checker_node);
                                
                                //node_start_layer.replaceChild(new_node, checker_node);
                            }
                            //console.log("REPLACED NOTE WITH A REST");
                        }

                        // exit the for loop, to save operations
                        else{
                            //console.log("break, resting start");
                            break;
                        }
                    }

                }

            }
            
        }

        // delete redundant notes at the ending measure
        else if(s_e == 1){
            var ending_layers = ending_measure_node.getElementsByTagName("layer");
           
            // go across all layers of the ending measure
            for (var lyrs = 0; lyrs < ending_layers.length; lyrs++){
 

                var node_end_layer = ending_layers[lyrs];
               
                // go through layer children
                for (var k = node_end_layer.childNodes.length - 1; k >= 0; k--){
                    //console.log("replacing with rests at end");
                    var checker_node = node_end_layer.childNodes[ k ];

                    if(checker_node.nodeType == 1){
                        //console.log("check node  " + k);

                        // if node is a chord, find one of its notes, and continue
                        if(checker_node.tagName == "chord"){
                            for (var im = 0; im < checker_node.childNodes.length; im++ ){

                                if(checker_node.childNodes[im].nodeType == 1){
                                    checker_node = checker_node.childNodes[im];
                                    break;
                                }
                            }

                        }
                        var checker_id = checker_node.getAttribute("xml:id");
                        var checker_time = vrvToolkit.getTimeForElement(checker_id);
                        //console.log(" time for the check :  "+ checker_id  + "......" + checker_time);
                        //console.log("pattern starts at " + patterns_start[pat_id]);
                        if(checker_time > patterns_end[pat_id] || checker_node.tagName == "rest"){
                            // replace whole chord
                            if(checker_node.parentNode.tagName == "chord"){
                                var checker_chord = checker_node.parentNode;
                                var previous_dur = checker_chord.getAttribute("dur");
                                // Create a rest (pause) element, with attribute dur = deleted elements dur
                                // and namespace = the parent namespace
                                var new_node = xmlDoc.createElementNS("http://www.music-encoding.org/ns/mei","rest" );
                                new_node.setAttribute("dur", previous_dur );
                                
                                // if chord is dotted, add dots to the rest aswell
                                if(checker_chord.getAttribute("dots") != null){
                                    var num_dots = checker_chord.getAttribute("dots");
                                    new_node.setAttribute("dots", num_dots );                             
                                }
                                node_end_layer.replaceChild(new_node, checker_chord);
                            }
                            // replace note
                            else{
                                var previous_dur = checker_node.getAttribute("dur");
                                // Create a rest (pause) element, with attribute dur = deleted elements dur
                                // and namespace = the parent namespace
                                var new_node = xmlDoc.createElementNS("http://www.music-encoding.org/ns/mei","rest" );
                                new_node.setAttribute("dur", previous_dur );
                                // if the note is dotted, add dots to the rest aswell
                                if(checker_node.getAttribute("dots") != null){
                                    var num_dots = checker_node.getAttribute("dots");
                                    new_node.setAttribute("dots", num_dots );                             
                                }
                                node_end_layer.replaceChild(new_node, checker_node);
                            }
                            //console.log("REPLACED NOTE WITH A REST");
                        }

                        // exit the for loop, to save operations
                        else{
                            //console.log("end it");
                            //console.log("break, resting end");
                            break;
                        }
                    
                    }

                }
            }
        }
    }

    //document.getElementById("demo").innerHTML = txt;
    var serializer = new XMLSerializer();
    var xmlString = serializer.serializeToString( xmlDoc );
    //console.log( "xmlString: \n" + xmlString );
    // send edited xml string to PHP
    // console.log(song_id);
    //console.log("timestamp", pat_timestamp);
    //console.log("realy?" + pat_rank);
    //$.ajax({
    //    url: 'static/php/post_xml_patterns.php',
    //    type: "POST",
    //    data: ({which_song: song_id,  xml_string_pattern: xmlString,  pattern_rank: pat_rank,  pattern_timestamp: pat_timestamp}),
    //    success: function(data){
    //        console.log(data);  //write out success message
    //    }
    //}); 
    
    $.ajax({
        url: '/ajax/post_xml_patterns/',
        datatype: 'json',
        type: "POST",
        data: {csrfmiddlewaretoken: window.CSRF_TOKEN, which_song: song_id,  xml_string_pattern: xmlString,  pattern_rank: pat_rank,  pattern_timestamp: pat_timestamp},
        success: function(result) {
            console.log("Call to post_xml_patterns succeded: ");
            console.log(result)
        },
        error: function(req, err){ 
            console.log("Call to post_xml_patterns failed; " + err);
        },
    });
}